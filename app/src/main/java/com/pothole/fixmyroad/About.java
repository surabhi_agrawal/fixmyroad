package com.pothole.fixmyroad;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.crittercism.app.Crittercism;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

//import android.R;
//package anubhav.hoolia;

public class About extends Activity{

    DatabaseHandler dbAbout = new DatabaseHandler(this);
    JSONObject jobject;
    String usTitle;
    String usDetails;
    String workTitle;
    String workDetails;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);


        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setIcon(R.drawable.ic_arrow_back_white_24dp);
        getActionBar().setHomeButtonEnabled(true);
        // Code Starts for checking Internet and GPS Enabled on Device
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.setMessage(this.getResources().getString(R.string.network_not_enabled));
            alert.setPositiveButton(this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            alert.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub


                }
            });
            alert.show();
        }


        // Code Ends for Checking Internet and GPS Enabled on Device
        int count = dbAbout.getCountAbout();
        try {
            if(count == 0 ||  dbAbout.getAboutConfigDate().compareTo(new Date(System.currentTimeMillis())) > MapsActivity.CACHE_EXPIRY_ABOUT_SCR ){

                try {
                    jobject = new GetAboutText().execute().get();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }
                try {
                    usTitle = jobject.getString("about_us_title");
                    usDetails = jobject.getString("about_us_detail");
                    workTitle = jobject.getString("how_it_works_title");
                    workDetails = jobject.getString("how_it_works_detail");
                    dbAbout.addAbout(usTitle,usDetails,workTitle,workDetails);
                    count = dbAbout.getCountAbout();
                    Conf conf  = new Conf(count);
                    dbAbout.addAboutConfig(conf);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }

            }

            else
            {
                String[] list = dbAbout.getAllAbout();
                usTitle = list[0];
                usDetails =list[1];
                workTitle = list[2];
                workDetails = list[3];
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
        }

        TextView ustitle = (TextView) findViewById(R.id.ustitle);
        TextView usdetails = (TextView) findViewById(R.id.usdetails);
        TextView worktitle = (TextView) findViewById(R.id.worktitle);
        TextView workdetail = (TextView) findViewById(R.id.workdetail);

        ustitle.setText(usTitle);
        usdetails.setText(usDetails);
        worktitle.setText(workTitle);
        workdetail.setText(workDetails);
    }


    class GetAboutText extends AsyncTask<Void,Void,JSONObject> {
        URL url;
        HttpURLConnection urlConnection;
        String result;
       // JSONArray jArray;
        JSONObject jObject;
        @Override
        protected JSONObject doInBackground(Void... params) {

            try {
                url = new URL("http://fixmyroad.in/init.json");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");

                try {
                    urlConnection.setDoOutput(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferReader.close();
                    result = stringBuilder.toString();
                    try{
                        // jArray = new JSONArray(result);
                        jObject= new JSONObject(result);
                        Log.d("GetAboutText Aysnc task:", jObject.toString());
                    }catch (JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Crittercism.logHandledException(e);
                    }
                    return jObject;

                }finally {
                    urlConnection.disconnect();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Crittercism.logHandledException(e);
                return null;
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}


