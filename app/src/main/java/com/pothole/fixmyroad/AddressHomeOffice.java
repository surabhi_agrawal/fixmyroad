package com.pothole.fixmyroad;//package com.anubhav.pothole.helloworld;
//
//import android.content.Context;
//import android.graphics.Point;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.Projection;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
///**
// * Created by hp on 07-Mar.
// */
//public class AddressHomeOffice extends FragmentActivity implements LocationListener, OnMapReadyCallback {
//
//    private GoogleMap gMap;
//    URL url;
//    HttpURLConnection httpURLConnection;
//    private ImageView mapMarker;
//    private int centerX = -1;
//    private int centerY = -1;
//    LatLng centerLatLng;
//    private float zoomLevel = 14;
//    private LocationManager locationManager;
//    String result;
//    Button btnHomeAddress;
//    private double latitude;
//    private double longitude;
//    DatabaseHandler db;
//
//
//@Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.address);
//
//        MapFragment mapFragment = (MapFragment) this.getFragmentManager().findFragmentById(R.id.mapAddress);
//        btnHomeAddress=(Button) findViewById(R.id.btnHome);
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        mapFragment.getMapAsync(this);
//
//        mapMarker = (ImageView) findViewById(R.id.drop_map_marker_icon_view);
//
//
//        btnHomeAddress.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                DisplayMetrics dm = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(dm);
//
//                int width = dm.widthPixels;
//                int height = dm.heightPixels;
//
//                centerX = width / 2;
//                centerY = (height / 2) /*+ (imageHeight/2)*/;
//
//                Log.d("tag", "onDrag(MotionEvent motionEvent)");
//                Projection projection = (gMap != null && gMap
//                        .getProjection() != null) ? gMap.getProjection()
//                        : null;
//
//                if (projection != null) {
//                    centerLatLng = projection.fromScreenLocation(new Point(
//                            centerX, centerY));
//
//                    Log.d("tag", "centerLatLng:" + centerLatLng.toString());
//                    latitude = centerLatLng.latitude;
//                    longitude = centerLatLng.longitude;
//                    try {
//                        JSONObject jObject = new GetGeoCoding().execute().get();
//                        Log.d("Home Address","" +jObject );
//                        JSONArray jArray = jObject.getJSONArray("results");
//                        JSONObject finalObject = jArray.getJSONObject(3);
//                        String address = finalObject.getString("formatted_address");
//                        Log.d("Home Address",address);
//                        if (db.getHomeAddressCount() == 0) {
//                        HomeAddress_DB homeAddress = new HomeAddress_DB(latitude, longitude, 1);
//                        Log.d("Home Lat long",homeAddress.homeLatitude + "," + homeAddress.homeLongitude + "," + homeAddress.flag);
//                        db.addHomeAddress(homeAddress);
//                        gMap.addMarker(new MarkerOptions().position(AddressHomeOffice.this.centerLatLng).snippet(address).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon)));
//                        return;
//                    }
//                    Log.d("NO. of rows Affected to 0", "" +AddressHomeOffice.this.db.updateHomeAddressFlag());
//                    db.addHomeAddress(new HomeAddress_DB(latitude, longitude, 1));
//                    gMap.clear();
//                    gMap.addMarker(new MarkerOptions().position(centerLatLng).snippet(address).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon)));
//                    return;
//                       // gMap.addMarker(new MarkerOptions().position(centerLatLng).snippet(address).icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon)));
//                    }catch(Exception e){
//                        e.printStackTrace();
//                    }
//                }
//                else {
//                    Log.d("tag", "projection == null");
//                }
//            }
//        });
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        gMap = googleMap;
//        //  gMap.setMyLocationEnabled(true);
////        LatLng loc1= new LatLng(12.9667, 77.5667);
////        zoomLevel = gMap.getCameraPosition().zoom;
////        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc1, 13));
////        gMap.addMarker(new MarkerOptions()
////                .position(loc1)
////                .title("Marker"));
//
//
//        LatLng loc1 = new LatLng(12.9667d, 77.5667d);
//        this.zoomLevel = this.gMap.getCameraPosition().zoom;
//        this.gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc1, 13.0f));
//        this.gMap.addMarker(new MarkerOptions().position(loc1).title("Marker"));
//        HomeAddress_DB home =this.db.getHomeAddress();
//        LatLng hLatLong = new LatLng(home.getHomeLatitude(), home.getHomeLongitude());
//        Log.d("Home read from DB", BuildConfig.FLAVOR + hLatLong);
//        this.gMap.addMarker(new MarkerOptions().position(hLatLong).snippet("Home").icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon)));
//
//    }
//
//
//    @Override
//    public void onLocationChanged(Location location) {
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
//        gMap.animateCamera(cameraUpdate);
//        locationManager.removeUpdates(this);
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
//
//    class GetGeoCoding extends AsyncTask<Void,Void,JSONObject> {
//        URL url;
//        HttpURLConnection urlConnection;
//        String result;
//        JSONArray jArray;
//        JSONObject jObject;
//        @Override
//        protected JSONObject doInBackground(Void... params) {
//            try {
//                // sample URL of reverse geocoding api
//                url = new URL("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude);
//
//                Log.d("URL ", url.toString());
//                httpURLConnection = (HttpURLConnection) url.openConnection();
//                httpURLConnection.setRequestMethod("GET");
//
//                try {
//                    httpURLConnection.setDoOutput(false);
//                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
//                    httpURLConnection.setRequestProperty("Accept", "application/json");
//                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String line;
//                    while ((line = bufferReader.readLine()) != null) {
//                        stringBuilder.append(line).append("\n");
//                    }
//                    bufferReader.close();
//                    result = stringBuilder.toString();
//                    Log.d("Json of Latlong", "" + result);
//                    try {
//                        jObject = new JSONObject(result);
//                    } catch (JSONException e) {
//                        Log.e("log_tag", "Error parsing data " + e.toString());
//                    }
//                    return jObject;
//                } finally {
//                    httpURLConnection.disconnect();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                return null;
//            }
//        }
//    }
//}
