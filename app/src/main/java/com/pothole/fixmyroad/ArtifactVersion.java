package com.pothole.fixmyroad;

/**
 * Created by hp on 8/3/2016.
 */
// THis class is for version checking and asking to update app
public interface ArtifactVersion extends Comparable<ArtifactVersion> {

    int getMajorVersion();

    int getMinorVersion();

    int getIncrementalVersion();

    int getBuildNumber();

    String getQualifier();

    void parseVersion(String version);
}

