package com.pothole.fixmyroad;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by hp on 8/9/2016.
 */
public class Conf {
    String timestamp;
    int count;

    public Conf()
    {

    }
    public Conf(int _count)
    {
        count = _count;
    }

    public void setTimestamp(String _timestamp) {
        this.timestamp = _timestamp;
    }

    public String getTimestamp() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCount(int _count)
    {
        this.count = _count;
    }

    public int getCount()
    {
        return this.count;
    }
}
