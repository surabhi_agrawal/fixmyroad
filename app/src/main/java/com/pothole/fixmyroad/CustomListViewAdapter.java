package com.pothole.fixmyroad;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * Created by hp on 7/29/2016.
 */
public class CustomListViewAdapter extends ArrayAdapter<RowItem_History> {
    private Activity activity;
    double Lat, Lon;

    public CustomListViewAdapter(Activity activity, int resource, List<RowItem_History> potholes) {
        super(activity, resource, potholes);
        this.activity = activity;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.list_history, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }

        RowItem_History pothole = getItem(position);
        Log.d("Each Pothole value", pothole.toString() + position);
        //  holder.Email.setText(pothole.getEmail());
        //  holder.Latitude.setText((int)pothole.getLatitude());


        // for getting current location of the device required for finding distance of the pothole from current location
        GPSTracker mGPS = new GPSTracker(activity);
        if (mGPS.canGetLocation) {
            mGPS.getLocation();
            Lat = mGPS.getLatitude();
            Lon = mGPS.getLongitude();
        } else {
            Log.d("GPS Tracker failed", "");
        }


        // Getting Area of Particular Lat Long
//        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
//        List<Address> addresses = null;
//        try {
//            addresses = geocoder.getFromLocation(pothole.getLatitude(), pothole.getLongitude(), 1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String cityName = addresses.get(0).getAddressLine(0);
//       // String stateName = addresses.get(0).getAddressLine(1);
//        //String countryName = addresses.get(0).getAddressLine(2);
//        holder.Area.setText(cityName);
        Log.d("Cheenu Timestmp: ", pothole.getTimeStamp());
//        Date date = new Date(pothole.getTimeStamp());
//        Log.d("DAte: ",date.toString());DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        try {
            Date serverDate = new Date(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse(pothole.getTimeStamp()).getTime());
            SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM, h:mm a");
            String formattedDate = dateFormat.format(serverDate);
            Log.d("Cheenu Date",formattedDate);
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
//        String formattedDate = formatter.format(pothole.getTimeStamp());
            holder.Timestamp.setText(formattedDate);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        double finalDistance = distance(Lat,Lon,pothole.getLatitude(),pothole.getLongitude());
        //holder.Distance.setText(Double.valueOf(pothole.getLongitude()).toString());
        holder.Distance.setText(new DecimalFormat("##.#").format(Double.valueOf(finalDistance)) + " kms away");
        //Log.d("getimageurl:", " "+pothole.getImageUrl());
        String imageUrl;
        imageUrl = pothole.getImageUrl();

        if (imageUrl != null)
        {
            if(imageUrl.contains("http")) {
//                Picasso.with(activity).load(imageUrl).resize(100, 100).into(holder.Image);
                Glide.with(activity)
                        .load(imageUrl)
//                        .override(100, 100)
//                        .fitCenter()//.centerCrop()
                        .into(holder.Image);
            }
            else{
                Glide.with(activity)
                        .load(new File(imageUrl))
//                        .override(100, 100)
//                        .fitCenter()//.centerCrop()
                        .into(holder.Image);
            }

        }
        else
        {
//            Picasso.with(activity).load().resize(100,100).into(holder.Image);
            Glide.with(activity)
                    .load(R.drawable.ic_no_camera_capture_picture_image)
//                    .override(100, 100)
//                    .fitCenter()//.centerCrop()
                    .into(holder.Image);

        }

        if((pothole.getAddress() == null) || (pothole.getAddress().equals("null")))
        {
            holder.Address.setText("");
        }
        else
        {
            holder.Address.setText(pothole.getAddress());
        }

        return convertView;
    }

    private static class ViewHolder {
       //private TextView Area;
        private TextView Timestamp;
        private TextView Distance;
        private ImageView Image;
        private TextView Address;

        public ViewHolder(View v) {
          //  Area = (TextView) v.findViewById(R.id.area);
            Image = (ImageView) v.findViewById(R.id.profile_pic);
            Timestamp = (TextView) v.findViewById(R.id.timestamp);
            Distance = (TextView)v.findViewById(R.id.distance);
            Address = (TextView)v.findViewById(R.id.address);
        }
    }

    public double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371; // in miles 3958.75, change to 6371 for kilometer output
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = (earthRadius * c);
        return dist; // output distance, in MILES }
    }
}
