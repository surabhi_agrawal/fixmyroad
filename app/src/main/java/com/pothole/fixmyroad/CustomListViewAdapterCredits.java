package com.pothole.fixmyroad;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by hp on 7/30/2016.
 */
public class CustomListViewAdapterCredits extends ArrayAdapter<RowItem_Credits> {
    private Activity activity;


    public CustomListViewAdapterCredits(Activity activity, int resource, List<RowItem_Credits> potholes) {
        super(activity, resource, potholes);
        this.activity = activity;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.list_credits, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }

        RowItem_Credits pothole= getItem(position);
        Log.d("Each Pothole value", pothole.toString() + position);


       holder.Name.setText(pothole.getEmail());
       holder.CountPotholes.setText(Integer.valueOf(pothole.getCount_potholes()).toString());

        return convertView;
    }

    private static class ViewHolder {
        private TextView Name;
        private TextView CountPotholes;

        public ViewHolder(View v) {
            Name = (TextView) v.findViewById(R.id.name);
            CountPotholes = (TextView) v.findViewById(R.id.countpotholes);
        }
    }
}
