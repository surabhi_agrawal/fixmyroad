package com.pothole.fixmyroad;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pothole.fixmyroad.MapWrapperLayout.OnDragListener;
import com.google.android.gms.maps.MapFragment;

//package anubhav.hoolia;

public class CustomMapFragment extends MapFragment {
    private MapWrapperLayout mMapWrapperLayout;
    private View mOriginalView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mOriginalView = super.onCreateView(inflater, container, savedInstanceState);
        this.mMapWrapperLayout = new MapWrapperLayout(getActivity());
        this.mMapWrapperLayout.addView(this.mOriginalView);
        return this.mMapWrapperLayout;
    }

    public View getView() {
        return this.mOriginalView;
    }

    public void setOnDragListener(OnDragListener onDragListener) {
        this.mMapWrapperLayout.setOnDragListener(onDragListener);
    }
}
