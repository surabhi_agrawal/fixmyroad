package com.pothole.fixmyroad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//package anubhav.hoolia;
//import com.oguzdev.circularfloatingactionmenu.library.BuildConfig;

public class DatabaseHandler extends SQLiteOpenHelper {
    public static final String DATABASE_FILE_PATH;
    private static final String DATABASE_NAME = "db_user_level_info";
    private static final int DATABASE_VERSION = 1;
    private static final String KEY_COMMENTS = "comments";
    private static final String KEY_DEVICEID = "deviceid";
    private static final String KEY_FLAG_HOME = "flag_home";
    private static final String KEY_FLAG_OFFICE = "flag_office";
    private static final String KEY_HOME_LATITUDE = "latitude_home";
    private static final String KEY_HOME_LONGITUDE = "longitude_home";
    private static final String KEY_ID = "id";
    private static final String KEY_ID_HOME = "id_home";
    private static final String KEY_ID_OFFICE = "id_office";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LONG = "longitude";
    private static final String KEY_OFFICE_LATITUDE = "latitude_office";
    private static final String KEY_OFFICE_LONGITUDE = "longitude_office";
    private static final String KEY_PATH_OF_IMG = "pathofimage";
    private static final String KEY_TIMESTAMP_HOME = "timestamp_home";
    private static final String KEY_TIMESTAMP_OFFICE = "timestamp_office";
    private static final String KEY_TIMESTP = "timestamp";
    private static final String TABLE_HOME_ADDRESS = "tb_home_address";
    private static final String TABLE_OFFICE_ADDRESS = "tb_office_address";
    private static final String TABLE_POTHOLE = "tb_user_spots";
    private static final String TABLE_CONFIG = "tb_config";
    private static final String KEY_CONFIG_ID = "id_config";
    private static final String KEY_TIMESTAMP = "last_updated";
    private static final String KEY_COUNT = "count";
    private static final String KEY_ISSYNC = "isSync";
    private static final String KEY_EMAILID = "email_id";

    //for Database variables for History class
    private static final String TABLE_USER_HISTORY = "tb_user_history";
    private static final String TABLE_HIST_CONFIG = "tb_hist_config";
    private static final String KEY_HIST_TIMESTP = "timestamp";
    private static final String KEY_HIST_LAT = "latitude";
    private static final String KEY_HIST_LONG = "longitude";
    private static final String KEY_HIST_PATH_OF_IMG = "pathofimage";
    private static final String KEY_HIST_EMAIL = "email_id";
    private static final String KEY_HIST_COUNT = "count";
    private static final String KEY_HIST_ADDRESS = "address";
    private static final String KEY_HIST_ISSYNC = "isSync";


    // Database variable for Credits class
    private static final String TABLE_USER_CREDITS = "tb_user_credits";
    private static final String TABLE_CREDITS_CONFIG = "tb_credits_config";
    private static final String KEY_CREDIT_EMAIL = "email_id";
    private static final String KEY_CREDIT_COUNT = "count";
    private static final String KEY_CREDIT_COUNT_POTHOLES = "count_potholes";

    // Database variables for About class
    private static final String TABLE_ABOUT = "tb_app_about";
    private static final String TABLE_CONFIG_ABOUT = "tb_config_about";
    private static final String KEY_ABOUT_TIMESTP = "timestamp";
    private static final String KEY_ABOUT_US_TITLE = "us_title";
    private static final String KEY_ABOUT_US_DETAILS = "us_detail";
    private static final String KEY_ABOUT_WORK_TITLE = "work_title";
    private static final String KEY_ABOUT_WORK_DETAILS  = "work_detail";
    private static final String KEY_ABOUT_COUNT = "count";



    static {
        DATABASE_FILE_PATH = Environment.getExternalStorageDirectory().toString();
    }

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        String CREATE_POTHOLE_TABLE = "CREATE TABLE tb_user_spots(id INTEGER PRIMARY KEY, timestamp DATETIME, latitude DOUBLE, longitude DOUBLE, pathofimage TEXT, comments TEXT, deviceid TEXT, email_id TEXT, isSync INTEGER);";
        Log.d("Table  ", CREATE_POTHOLE_TABLE);
        db.execSQL(CREATE_POTHOLE_TABLE);
        String CREATE_HOME_ADDRESS_TABLE = "CREATE TABLE tb_home_address(id_home INTEGER PRIMARY KEY, timestamp_home TEXT, latitude_home DOUBLE, longitude_home DOUBLE, flag_home INTEGER);";
        Log.d("Table ", CREATE_HOME_ADDRESS_TABLE);
        db.execSQL(CREATE_HOME_ADDRESS_TABLE);
        String CREATE_OFFICE_ADDRESS_TABLE = "CREATE TABLE tb_office_address(id_office INTEGER PRIMARY KEY, latitude_office TEXT, longitude_office DOUBLE, flag_office INTEGER);";
        Log.d("Table ", CREATE_OFFICE_ADDRESS_TABLE);
        db.execSQL(CREATE_OFFICE_ADDRESS_TABLE);
        String CREATE_CONFIG_TABLE = "CREATE TABLE tb_config(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_CONFIG_TABLE);
        Log.d("Table ", CREATE_CONFIG_TABLE);

        // for History Class
        String CREATE_HISTORY_TABLE = "CREATE TABLE tb_user_history(id INTEGER PRIMARY KEY, timestamp DATETIME, latitude DOUBLE, longitude DOUBLE, pathofimage TEXT, comments TEXT, deviceid TEXT, email_id TEXT, address TEXT, isSync INTEGER);";
        Log.d("Table  ", CREATE_HISTORY_TABLE);
        db.execSQL(CREATE_HISTORY_TABLE);
        String CREATE_HIST_CONFIG_TABLE = "CREATE TABLE tb_hist_config(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_HIST_CONFIG_TABLE);
        Log.d("Table ", CREATE_HIST_CONFIG_TABLE);

        // for Credits Class
        String CREATE_CREDITS_TABLE = "CREATE TABLE tb_user_credits(id INTEGER PRIMARY KEY, count_potholes INTEGER,email_id TEXT);";
        Log.d("Table  ", CREATE_CREDITS_TABLE);
        db.execSQL(CREATE_CREDITS_TABLE);
        String CREATE_CREDITS_CONFIG_TABLE = "CREATE TABLE tb_credits_config(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_CREDITS_CONFIG_TABLE);
        Log.d("Table ", CREATE_CREDITS_CONFIG_TABLE);

        // for About Class
        String CREATE_ABOUT_TABLE = "CREATE TABLE tb_app_about(id INTEGER PRIMARY KEY, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, us_title TEXT, us_detail TEXT, work_title TEXT, work_detail TEXT);";
        Log.d("Table  ", CREATE_ABOUT_TABLE);
        db.execSQL(CREATE_ABOUT_TABLE);
        String CREATE_ABOUT_CONFIG_TABLE = "CREATE TABLE tb_config_about(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_ABOUT_CONFIG_TABLE);
        Log.d("Table ", CREATE_ABOUT_CONFIG_TABLE);


    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tb_user_spots");
        db.execSQL("DROP TABLE IF EXISTS tb_home_address");
        db.execSQL("DROP TABLE IF EXISTStb_office_address");
        db.execSQL("DROP TABLE IF EXISTS tb_config");
        db.execSQL("DROP TABLE IF EXISTS tb_user_history");
        db.execSQL("DROP TABLE IF EXISTS tb_hist_config");
        db.execSQL("DROP TABLE IF EXISTS tb_user_credits");
        db.execSQL("DROP TABLE IF EXISTS tb_credits_config");
        db.execSQL("DROP TABLE IF EXISTS tb_app_about");
        db.execSQL("DROP TABLE IF EXISTS tb_config_about");
        onCreate(db);
        db.close();

    }

    void addPothole(Potholes_DB pothole) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TIMESTP, pothole.getTimestamp());
        values.put(KEY_LAT, pothole.getlatitude());
        values.put(KEY_LONG, pothole.getlongitude());
        values.put(KEY_PATH_OF_IMG, pothole.getPathofImage());
        values.put(KEY_COMMENTS, pothole.getComments());
        values.put(KEY_DEVICEID, pothole.getDeviceid());
        values.put(KEY_EMAILID,pothole.getEmailId());
        values.put(KEY_ISSYNC, pothole.getIsSync());
        db.insert(TABLE_POTHOLE, null, values);
        db.close();
    }


    void addConfig(Conf conf)
    {
        SQLiteDatabase dbConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
       // values.put(KEY_TIMESTAMP,conf.getTimestamp());
        values.put(KEY_COUNT,conf.getCount());
        dbConfig.insert(TABLE_CONFIG,null,values);
        dbConfig.close();
    }


    void addHomeAddress(HomeAddress_DB homeAddress) {
        SQLiteDatabase dbHome = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TIMESTAMP_HOME, homeAddress.getTimestamp());
        values.put(KEY_HOME_LATITUDE, Double.valueOf(homeAddress.getHomeLatitude()));
        values.put(KEY_HOME_LONGITUDE, Double.valueOf(homeAddress.getHomeLongitude()));
        values.put(KEY_FLAG_HOME, Integer.valueOf(homeAddress.getHomeFlag()));
        Log.d("Values to be inserted", BuildConfig.FLAVOR + values);
        dbHome.insert(TABLE_HOME_ADDRESS, null, values);
        dbHome.close();
    }

    void addOfficeAddress(OfficeAddress_DB officeAddress) {
        SQLiteDatabase dbOffice = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TIMESTAMP_OFFICE, officeAddress.getTimestamp());
        values.put(KEY_OFFICE_LATITUDE, Double.valueOf(officeAddress.getHomeLatitude()));
        values.put(KEY_OFFICE_LONGITUDE, Double.valueOf(officeAddress.getHomeLongitude()));
        values.put(KEY_FLAG_OFFICE, Boolean.valueOf(officeAddress.getFlag()));
        dbOffice.insert(TABLE_OFFICE_ADDRESS, null, values);
        dbOffice.close();
    }

//    Potholes_DB getPothole(int id) {
//        SQLiteDatabase db = getReadableDatabase();
//        String str = TABLE_POTHOLE;
//        String[] strArr = new String[]{KEY_ID, KEY_TIMESTP, KEY_LAT, KEY_LONG, KEY_PATH_OF_IMG, KEY_COMMENTS, KEY_DEVICEID};
//        String[] strArr2 = new String[DATABASE_VERSION];
//        strArr2[0] = String.valueOf(id);
//        Cursor cursor = db.query(str, strArr, "id=?", strArr2, null, null, null, null);
//        if (cursor != null) {
//            cursor.moveToFirst();
//        }
//        return new Potholes_DB(cursor.getDouble(2), cursor.getDouble(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
//    }

    HomeAddress_DB getHomeAddress() {
        SQLiteDatabase db = getReadableDatabase();
        String str = TABLE_HOME_ADDRESS;
        String[] strArr = new String[]{KEY_TIMESTAMP_HOME, KEY_HOME_LATITUDE, KEY_HOME_LONGITUDE, KEY_FLAG_HOME};
        String[] strArr2 = new String[DATABASE_VERSION];
        strArr2[0] = String.valueOf(DATABASE_VERSION);
        Cursor cursor = db.query(str, strArr, "flag_home=?", strArr2, null, null, null, null);
        Log.d("Cursor", BuildConfig.FLAVOR + cursor);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        HomeAddress_DB homeAddress = new HomeAddress_DB(cursor.getDouble(DATABASE_VERSION), cursor.getDouble(2), cursor.getInt(3));
        Log.d("Home Address", BuildConfig.FLAVOR + homeAddress.homeLatitude + "," + homeAddress.homeLongitude + "," + homeAddress.flag);
        cursor.close();
        db.close();
        return homeAddress;
    }

//    int updateHomeAddressFlag() {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_FLAG_HOME, Integer.valueOf(0));
//        return db.update(TABLE_HOME_ADDRESS, values, null, null);
//    }

    public List<Potholes_DB> getAllPotholes() {
        List<Potholes_DB> potholeList = new ArrayList();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM tb_user_spots", null);
        if (cursor.moveToFirst()) {
            do {
                Potholes_DB pothole = new Potholes_DB();
                pothole.setID(Integer.parseInt(cursor.getString(0)));
                pothole.setTimestamp(cursor.getString(1));
                pothole.setlatitude(cursor.getDouble(2));
                pothole.setlongitude(cursor.getDouble(3));
                pothole.setPathofImage(cursor.getString(4));
                pothole.setComments(cursor.getString(5));
                pothole.setDeviceID(cursor.getString(6));
                pothole.setEmailId(cursor.getString(7));
                potholeList.add(pothole);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return potholeList;
    }

    public int getHomeAddressCount() {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_home_address", null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
//
//    public int getPotholesCount() {
//        Cursor cursor = getReadableDatabase().rawQuery("SELECT  * FROM tb_user_spots", null);
//        cursor.close();
//        return cursor.getCount();
//    }

    public int getConfigCount(){
//        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_config",null);

        Cursor cursor = getReadableDatabase().rawQuery("SELECT count FROM tb_config WHERE ID_CONFIG = (SELECT MAX(ID_CONFIG) FROM tb_config)",null);
        int count = cursor.getCount();
        cursor.close();
        return count;

    }


    public Date getConfigDate() throws ParseException
    {

        // for testing purpose start
//        String selectQuery = "SELECT  * FROM " + TABLE_CONFIG;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor1 = db.rawQuery(selectQuery, null);
//        if(cursor1.moveToFirst()){
//            do {
//                Log.d("Cheenu id", cursor1.getInt(0) + "");
//                Log.d("Cheenu count", cursor1.getInt(1) + "");
//                Log.d("Cheenu last_updated", cursor1.getString(2));
//            }while(cursor1.moveToNext());
//        }
//        cursor1.close();
// for testing purpose End

        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_config",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
       // Log.d("Cheenu cursorDate: ",cursorDate);
        Date date = formatter.parse(cursorDate);

        cursor.close();
        return date;
    }

    public int getCountPothole(String email)
    {
        Cursor cursor = getReadableDatabase().rawQuery("Select count(*) from tb_user_spots where email_id =" + email,null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }


    public List<Potholes_DB> getPotholesIsSync() {
        List<Potholes_DB> potholeList = new ArrayList();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM tb_user_spots where isSync = 0" , null);

        if (cursor.moveToFirst()) {
            do {
                Potholes_DB pothole = new Potholes_DB();
                pothole.setID(Integer.parseInt(cursor.getString(0)));
                pothole.setTimestamp(cursor.getString(1));
                pothole.setlatitude(cursor.getDouble(2));
                pothole.setlongitude(cursor.getDouble(3));
                pothole.setPathofImage(cursor.getString(4));
                pothole.setComments(cursor.getString(5));
                pothole.setDeviceID(cursor.getString(6));
                pothole.setEmailId(cursor.getString(7));
                potholeList.add(pothole);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return potholeList;
    }

    public void updatePotholeSynced()
    {
//        Cursor cursor = getWritableDatabase().rawQuery("UPDATE tb_user_spots set isSync = 1 where isSync = 0",null);
//        int count= cursor.getCount();
//        Log.d("count of pothole",count +"");
//        cursor.close();
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_ISSYNC,Integer.valueOf(1));
        int count = db.update(TABLE_POTHOLE,cv,KEY_ISSYNC + "=" + Integer.valueOf(0),null);
        Log.d("count of pothole",count +"");

        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM tb_user_spots where isSync = 0" , null);

        Log.d("Sync",cursor.getCount()+ "");

        db.close();
        cursor.close();
    }


    // Methods for History Class

    void addHistoryPothole(RowItem_History pothole) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_HIST_TIMESTP, pothole.getTimeStamp());
        values.put(KEY_HIST_LAT, Double.valueOf(pothole.getLatitude()));
        values.put(KEY_HIST_LONG, Double.valueOf(pothole.getLongitude()));
        values.put(KEY_HIST_PATH_OF_IMG, pothole.getImageUrl());
        values.put(KEY_HIST_EMAIL,pothole.getEmail());
        values.put(KEY_HIST_ADDRESS,pothole.getAddress());
        values.put(KEY_HIST_ISSYNC,pothole.getIsSync());
        db.insert(TABLE_USER_HISTORY, null, values);
        db.close();
    }

    void addHistoryConfig(Conf conf)
    {
        SQLiteDatabase dbConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTAMP,conf.getTimestamp());
        values.put(KEY_HIST_COUNT,conf.getCount());
        dbConfig.insert(TABLE_HIST_CONFIG,null,values);
        dbConfig.close();
    }


    public int getCountHistory()
    {
//        Cursor cursor = getReadableDatabase().rawQuery("SELECT count(*) FROM tb_user_history where isSync = 1",null);
        Cursor cursor = getReadableDatabase().rawQuery("SELECT count(*) FROM tb_user_history where isSync = 1 ",null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        Log.d("count of rows",count +"");
        cursor.close();
        return count;
    }


    public List<RowItem_History> getAllHistory() {
        List<RowItem_History> historyList = new ArrayList<RowItem_History>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USER_HISTORY + " ORDER BY " + KEY_HIST_TIMESTP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RowItem_History history = new RowItem_History();
                history.setTimeStamp(cursor.getString(1));
                history.setLatitude(cursor.getDouble(2));
                history.setLongitude(cursor.getDouble(3));
                history.setImageUrl(cursor.getString(4));
                history.setEmail(cursor.getString(7));
                history.setAddress(cursor.getString(8));
                // Adding contact to list
                historyList.add(history);

                for (int i = 1; i < 9;i++)
                    Log.d("History Data:" , i + ": "+  cursor.getString(i));
            } while (cursor.moveToNext());
        }

        db.close();
        cursor.close();
        // return contact list
        return historyList;
    }
    public Date getHistConfigDate() throws ParseException
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_hist_config",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
        Date date = formatter.parse(cursorDate);
        cursor.close();
        return date;
    }


    void addCredits(RowItem_Credits credits) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CREDIT_COUNT_POTHOLES, credits.getCount_potholes());
        values.put(KEY_CREDIT_EMAIL,credits.getEmail());
        db.insert(TABLE_USER_CREDITS, null, values);
        db.close();
    }

    void addCreditsConfig(Conf conf)
    {
        SQLiteDatabase dbConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTAMP,conf.getTimestamp());

        values.put(KEY_CREDIT_COUNT,conf.getCount());
        dbConfig.insert(TABLE_CREDITS_CONFIG,null,values);
        dbConfig.close();
    }

    public Date getCreditConfigDate() throws ParseException
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_credits_config",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
        Date date = formatter.parse(cursorDate);

        cursor.close();
        return date;
    }

    public int getCountCredits()
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_user_credits",null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public List<RowItem_Credits> getAllCredit() {
        List<RowItem_Credits> creditList = new ArrayList<RowItem_Credits>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USER_CREDITS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RowItem_Credits credits = new RowItem_Credits();
                credits.setCount_potholes(cursor.getInt(1));
                credits.setEmail(cursor.getString(2));
                // Adding contact to list
                creditList.add(credits);
            } while (cursor.moveToNext());
        }

        db.close();
        cursor.close();
        // return contact list
        return creditList;
    }

    // About method
    void addAbout(String ustitle, String usdetails, String worktitle, String workdetails) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTP, getTimeStamp());
        values.put(KEY_ABOUT_US_TITLE,ustitle);
        values.put(KEY_ABOUT_US_DETAILS,usdetails);
        values.put(KEY_ABOUT_WORK_TITLE,worktitle);
        values.put(KEY_ABOUT_WORK_DETAILS,workdetails);
        db.insert(TABLE_ABOUT, null, values);
        db.close();
    }

    void addAboutConfig(Conf conf)
    {
        SQLiteDatabase dbAboutConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTAMP,conf.getTimestamp());
        values.put(KEY_ABOUT_COUNT,conf.getCount());
        dbAboutConfig.insert(TABLE_CONFIG_ABOUT,null,values);
        dbAboutConfig.close();
    }

    public int getCountAbout()
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_app_about",null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }


    public String[] getAllAbout() {
        String [] list = new String[4];
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ABOUT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                list = new String[]{cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5)};
                // Adding contact to list

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        // return contact list
        return list;
    }

    public Date getAboutConfigDate() throws ParseException
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_config_about",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
        Date date = formatter.parse(cursorDate);

        cursor.close();
        return date;
    }

    public ArrayList<RowItem_History> getAllLocalPothole()
    {
        ArrayList<RowItem_History> listAll = new ArrayList<RowItem_History>();

        String selectQuery = "SELECT * FROM " + TABLE_POTHOLE + " where " + KEY_ISSYNC + " = 0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst())
        {
            do
            {
            RowItem_History pothole = new RowItem_History();
            pothole.setTimeStamp(cursor.getString(1));
                pothole.setLatitude(cursor.getDouble(2));
                pothole.setLongitude(cursor.getDouble(3));
                pothole.setImageUrl(cursor.getString(4));
                pothole.setEmail(cursor.getString(7));
                pothole.setAddress("");
                // Adding contact to list
                listAll.add(pothole);
            } while (cursor.moveToNext());


        }
        db.close();
        cursor.close();
        return listAll;
    }



    int deleteAllHistory()
    {
       SQLiteDatabase db = getWritableDatabase();
       int count = db.delete(TABLE_USER_HISTORY,"1",null);
       db.close();
       return count;
    }

    int deleteAllPothole()
    {
        SQLiteDatabase db = getWritableDatabase();
        int isSync = 1;
        int count = db.delete(TABLE_POTHOLE,KEY_ISSYNC + "=" + isSync ,null);
        db.close();
        return count;
    }





//    public List<RowItem_History> getAllPotholeEmail(String emailID)
//    {
//        List<RowItem_History> listAll = new ArrayList<RowItem_History>();
//        String selectQuery = "SELECT  * FROM tb_user_spots where email_id =" + emailID ;
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                RowItem_History pothole= new RowItem_History();
//                pothole.setTimeStamp(cursor.getString(1));
//                pothole.setLatitude(cursor.getDouble(2));
//                pothole.setLongitude(cursor.getDouble(3));
//                pothole.setImageUrl(cursor.getString(4));
//                pothole.setEmail(cursor.getString(7));
//                pothole.setAddress("");
//                // Adding contact to list
//                listAll.add(pothole);
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return listAll;
//
//    }

    public int getCountPotholeIsSync()
    {
//        Cursor cursor = getReadableDatabase().rawQuery("SELECT count(*) FROM tb_user_history where isSync = 1",null);
        Cursor cursor = getReadableDatabase().rawQuery("SELECT count(*) FROM tb_user_spots where isSync = 1",null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        Log.d("count of rows",count +"");
        cursor.close();
        return count;
    }




// One time code for getting particular user id data from ones mobile
//    public void SetIsSyncToZero()
//    {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put(KEY_ISSYNC,Integer.valueOf(0));
//        int count = db.update(TABLE_POTHOLE,cv,KEY_EMAILID + " = ? ",new String[]{"agrawalsurabhi89@gmail.com"});
//
//        Log.d("count of pothole marked as 0",count +"");
//
//        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM tb_user_spots where isSync = 0" , null);
//
//        Log.d("ISSync to 0",cursor.getCount()+ "");
//
//        db.close();
//        cursor.close();
//
//    }


}
