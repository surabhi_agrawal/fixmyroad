package com.pothole.fixmyroad;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hp on 9/15/2016.
 */
public class DatabaseHandler_About extends SQLiteOpenHelper {
    public static final String DATABASE_FILE_PATH_ABOUT;
    private static final String DATABASE_NAME_ABOUT = "db_app_about";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_ABOUT = "tb_app_about";
    private static final String TABLE_CONFIG_ABOUT = "tb_config_about";
    private static final String KEY_ABOUT_TIMESTP = "timestamp";
    private static final String KEY_ABOUT_US_TITLE = "us_title";
    private static final String KEY_ABOUT_US_DETAILS = "us_detail";
    private static final String KEY_ABOUT_WORK_TITLE = "work_title";
    private static final String KEY_ABOUT_WORK_DETAILS  = "work_detail";
    private static final String KEY_ABOUT_COUNT = "count";

    static {
        DATABASE_FILE_PATH_ABOUT = Environment.getExternalStorageDirectory().toString();
    }
    public DatabaseHandler_About(About context) {
        super(context, DATABASE_NAME_ABOUT, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ABOUT_TABLE = "CREATE TABLE tb_app_about(id INTEGER PRIMARY KEY, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, us_title TEXT, us_detail TEXT, work_title TEXT, work_detail TEXT);";
        Log.d("Table  ", CREATE_ABOUT_TABLE);
        db.execSQL(CREATE_ABOUT_TABLE);
        String CREATE_ABOUT_CONFIG_TABLE = "CREATE TABLE tb_config_about(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_ABOUT_CONFIG_TABLE);
        Log.d("Table ", CREATE_ABOUT_CONFIG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tb_app_about");
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS tb_config_about");
        onCreate(db);
    }

    void addAbout(String ustitle, String usdetails, String worktitle, String workdetails) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
       // values.put(KEY_TIMESTP, getTimeStamp());
        values.put(KEY_ABOUT_US_TITLE,ustitle);
        values.put(KEY_ABOUT_US_DETAILS,usdetails);
        values.put(KEY_ABOUT_WORK_TITLE,worktitle);
        values.put(KEY_ABOUT_WORK_DETAILS,workdetails);
        db.insert(TABLE_ABOUT, null, values);
        db.close();
    }
    void addAboutConfig(Conf conf)
    {
        SQLiteDatabase dbAboutConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTAMP,conf.getTimestamp());
        values.put(KEY_ABOUT_COUNT,conf.getCount());
        dbAboutConfig.insert(TABLE_CONFIG_ABOUT,null,values);
        dbAboutConfig.close();
    }
    public int getCountAbout()
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_app_about",null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    // Getting All Contacts
    public String[] getAllAbout() {
        String [] list = new String[4];
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ABOUT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                list = new String[]{cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5)};
                // Adding contact to list

            } while (cursor.moveToNext());
        }

        // return contact list
        return list;
    }
    public Date getAboutConfigDate() throws ParseException
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_config_about",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
        Date date = formatter.parse(cursorDate);

        cursor.close();
        return date;
    }

}
