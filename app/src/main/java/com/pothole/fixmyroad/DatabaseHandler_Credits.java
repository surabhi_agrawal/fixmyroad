package com.pothole.fixmyroad;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hp on 9/15/2016.
 */
public class DatabaseHandler_Credits extends SQLiteOpenHelper {
    public static final String DATABASE_FILE_PATH_CREDITS;
    private static final String DATABASE_NAME_CREDITS = "db_user_level_credits";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_USER_CREDITS = "tb_user_credits";
    private static final String TABLE_CREDITS_CONFIG = "tb_credits_config";
    private static final String KEY_CREDIT_EMAIL = "email_id";
    private static final String KEY_CREDIT_COUNT = "count";
    private static final String KEY_CREDIT_COUNT_POTHOLES = "count_potholes";

    static {
        DATABASE_FILE_PATH_CREDITS = Environment.getExternalStorageDirectory().toString();
    }
    public DatabaseHandler_Credits(ListViewActivityCredits context) {
        super(context, DATABASE_NAME_CREDITS, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CREDITS_TABLE = "CREATE TABLE tb_user_credits(id INTEGER PRIMARY KEY, count_potholes INTEGER,email_id TEXT);";
        Log.d("Table  ", CREATE_CREDITS_TABLE);
        db.execSQL(CREATE_CREDITS_TABLE);
        String CREATE_CREDITS_CONFIG_TABLE = "CREATE TABLE tb_credits_config(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_CREDITS_CONFIG_TABLE);
        Log.d("Table ", CREATE_CREDITS_CONFIG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tb_user_credits");
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS tb_credits_config");
        onCreate(db);
    }


    void addCredits(RowItem_Credits credits) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CREDIT_COUNT_POTHOLES, credits.getCount_potholes());
        values.put(KEY_CREDIT_EMAIL,credits.getEmail());
        db.insert(TABLE_USER_CREDITS, null, values);
        db.close();
    }


    void addCreditsConfig(Conf conf)
    {
        SQLiteDatabase dbConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTAMP,conf.getTimestamp());

        values.put(KEY_CREDIT_COUNT,conf.getCount());
        dbConfig.insert(TABLE_CREDITS_CONFIG,null,values);
        dbConfig.close();
    }
    public Date getCreditConfigDate() throws ParseException
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_credits_config",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
        Date date = formatter.parse(cursorDate);

        cursor.close();
        return date;
    }

    public int getCount()
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_user_credits",null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public List<RowItem_Credits> getAllCredit() {
        List<RowItem_Credits> creditList = new ArrayList<RowItem_Credits>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USER_CREDITS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RowItem_Credits credits = new RowItem_Credits();
                credits.setCount_potholes(cursor.getInt(1));
                credits.setEmail(cursor.getString(2));
                // Adding contact to list
                creditList.add(credits);
            } while (cursor.moveToNext());
        }

        // return contact list
        return creditList;
    }

}
