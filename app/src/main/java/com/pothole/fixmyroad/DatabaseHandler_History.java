package com.pothole.fixmyroad;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hp on 9/14/2016.
 */
public class DatabaseHandler_History extends SQLiteOpenHelper {
    public static final String DATABASE_FILE_PATH_HISTORY;
    private static final String DATABASE_NAME_HISTORY = "db_user_level_history";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_USER_HISTORY = "tb_user_history";
    private static final String TABLE_HIST_CONFIG = "tb_hist_config";
    private static final String KEY_HIST_TIMESTP = "timestamp";
    private static final String KEY_HIST_LAT = "latitude";
    private static final String KEY_HIST_LONG = "longitude";
    private static final String KEY_HIST_PATH_OF_IMG = "pathofimage";
    private static final String KEY_HIST_EMAIL = "email_id";
    private static final String KEY_HIST_COUNT = "count";
    private static final String KEY_HIST_ADDRESS = "address";

    static {
        DATABASE_FILE_PATH_HISTORY = Environment.getExternalStorageDirectory().toString();
    }
    public DatabaseHandler_History(ListViewActivity context) {
        super(context, DATABASE_NAME_HISTORY, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HISTORY_TABLE = "CREATE TABLE tb_user_history(id INTEGER PRIMARY KEY, timestamp TEXT, latitude DOUBLE, longitude DOUBLE, pathofimage TEXT, comments TEXT, deviceid TEXT, email_id TEXT, address TEXT);";
        Log.d("Table  ", CREATE_HISTORY_TABLE);
        db.execSQL(CREATE_HISTORY_TABLE);
        String CREATE_HIST_CONFIG_TABLE = "CREATE TABLE tb_hist_config(id_config INTEGER PRIMARY KEY,count INTEGER, last_updated DATETIME DEFAULT CURRENT_TIMESTAMP );";
        db.execSQL(CREATE_HIST_CONFIG_TABLE);
        Log.d("Table ", CREATE_HIST_CONFIG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tb_user_history");
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS tb_hist_config");
        onCreate(db);
    }

    void addPothole(RowItem_History pothole) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_HIST_TIMESTP, pothole.getTimeStamp());
        values.put(KEY_HIST_LAT, Double.valueOf(pothole.getLatitude()));
        values.put(KEY_HIST_LONG, Double.valueOf(pothole.getLongitude()));
        values.put(KEY_HIST_PATH_OF_IMG, pothole.getImageUrl());
        values.put(KEY_HIST_EMAIL,pothole.getEmail());
        values.put(KEY_HIST_ADDRESS,pothole.getAddress());
        db.insert(TABLE_USER_HISTORY, null, values);
        db.close();
    }


    void addHistoryConfig(Conf conf)
    {
        SQLiteDatabase dbConfig = getWritableDatabase();
        ContentValues values = new ContentValues();
        // values.put(KEY_TIMESTAMP,conf.getTimestamp());
        values.put(KEY_HIST_COUNT,conf.getCount());
        dbConfig.insert(TABLE_HIST_CONFIG,null,values);
        dbConfig.close();
    }
        public int getCount()
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM tb_user_history",null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }



    // Getting All Contacts
    public List<RowItem_History> getAllHistory() {
        List<RowItem_History> historyList = new ArrayList<RowItem_History>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USER_HISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RowItem_History history = new RowItem_History();
                history.setTimeStamp(cursor.getString(1));
                history.setLatitude(cursor.getDouble(2));
                history.setLongitude(cursor.getDouble(3));
                history.setImageUrl(cursor.getString(4));
                history.setEmail(cursor.getString(7));
                history.setAddress(cursor.getString(8));
                // Adding contact to list
                historyList.add(history);
            } while (cursor.moveToNext());
        }

        // return contact list
        return historyList;
    }
//
//    public boolean databaseExist()
//    {
//        File dbFile = new File(DATABASE_FILE_PATH_HISTORY + "/" +DATABASE_NAME_HISTORY);
//        if (dbFile.exists() == true)
//            return true;
//        else
//            return false;
//    }
    public Date getHistConfigDate() throws ParseException
    {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT MAX(last_updated) FROM tb_hist_config",null);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cursor.moveToFirst();
        String cursorDate= cursor.getString(0);
        Date date = formatter.parse(cursorDate);

        cursor.close();
        return date;
    }

}
