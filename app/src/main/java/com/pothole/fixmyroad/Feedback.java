package com.pothole.fixmyroad;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crittercism.app.Crittercism;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * Created by hp on 8/4/2016.
 */
public class Feedback extends Activity{
    double Lat, Lon;
    ProgressDialog dialog;
    EditText editName,editPhone,editEmail,editFeedBackBody;
    String strLat, strLong;
    JSONObject potholes;
    String possibleEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_final);
        getActionBar().setIcon(R.drawable.ic_arrow_back_white_24dp);
        getActionBar().setHomeButtonEnabled(true);
//        TextView txtTitle = (TextView) findViewById(R.id.TextViewTitle);

        editName = (EditText)findViewById(R.id.firstName);
        editPhone = (EditText) findViewById(R.id.phoneNumber);
        editEmail = (EditText) findViewById(R.id.email);
        // final Spinner feedbackSpinner = (Spinner) findViewById(R.id.SpinnerFeedbackType);
       editFeedBackBody = (EditText) findViewById(R.id.feedback);
        Button btnSend = (Button) findViewById(R.id.submitButton);

        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Pattern phonePattern = Patterns.PHONE;

        Account[] accounts = AccountManager.get(this).getAccounts();
        // PERSMISSION ALERT: Account requires permission <uses-permission android:name="android.permission.GET_ACCOUNTS" />

        for (Account account : accounts) {
            Log.d("account",account.toString());
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;

            }

        }
        editEmail.setText(possibleEmail);
        // getting data from respective textboxes and spinners

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Checking if internet and GPS is on or not if not it will ask you to Enable it
                LocationManager lm = (LocationManager) Feedback.this.getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {
                    Crittercism.logHandledException(ex);
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch(Exception ex) {
                    Crittercism.logHandledException(ex);
                }

                if(!gps_enabled && !network_enabled) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(Feedback.this);
                    dialog.setMessage(Feedback.this.getResources().getString(R.string.gps_network_not_enabled));
                    dialog.setPositiveButton(Feedback.this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    dialog.setNegativeButton(Feedback.this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub


                        }
                    });
                    dialog.show();
                }


                AlertDialog.Builder alert = new AlertDialog.Builder(Feedback.this);
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                // Check if Internet present
                if (!cd.isConnectingToInternet()) {
                    // Internet Connection is not present
                    alert.setMessage(Feedback.this.getResources().getString(R.string.network_not_enabled));
                    alert.setPositiveButton(Feedback.this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    alert.setNegativeButton(Feedback.this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    alert.show();
                }

                final String name = editName.getText().toString();
                final String phone = editPhone.getText().toString();

                //  String feedbackType = feedbackSpinner.getSelectedItem().toString();
                final String feedback = editFeedBackBody.getText().toString();
                boolean is_feedback_input_validated = true;

                if(!isValidateString(name)) {
                    editName.setError("Enter Name");
                    is_feedback_input_validated = false;
                }
                if (!isValidateString(phone)) {
                    editPhone.setError("Enter Phone Number");
                    is_feedback_input_validated = false;
                }
                if (!isValidatePhone(phone))
                {
                    editPhone.setError("Enter 10 digit phone number");
                    is_feedback_input_validated = false;
                }
                if (!isValidateString(feedback)) {
                    editFeedBackBody.setError("Enter Feedback");
                    is_feedback_input_validated = false;
                }

                if (is_feedback_input_validated == true) {
                    final JSONObject jsonParent = new JSONObject();
                    JSONObject jsonChild = new JSONObject();

                    GPSTracker mGPS = new GPSTracker(Feedback.this);
                    if (mGPS.canGetLocation) {
                        mGPS.getLocation();
                        Lat = mGPS.getLatitude();
                        strLat = Double.toString(Lat);
                        Lon = mGPS.getLongitude();
                        strLong = Double.toString(Lon);
                    } else {
                        Log.d("GPS Tracker failed", "");
                    }

                    Log.d("Values of text", name + phone + possibleEmail + Lat + Lon);
                    try {
                        jsonChild.put("name", name);
                        jsonChild.put("email", possibleEmail);
                        jsonChild.put("phone", phone);
                        // jsonChild.put("ratetext",feedbackType);
                        //     jsonChild.put("review",feedback);
                        jsonChild.put("lat", strLat);
                        jsonChild.put("long", strLong);
                        jsonChild.put("parent", "app");
                        jsonChild.put("source", "contactUs");
                        jsonParent.put("review", jsonChild);
                        Log.d("Jsonobject of Feedback", jsonChild.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crittercism.logHandledException(e);
                    }

                    FeedbackUpload upload = new FeedbackUpload();
                    upload.execute(String.valueOf(jsonParent));
                    Intent intent= new Intent(Feedback.this,MapsActivity.class);
//                intent.putExtra("jsonpotholes",potholes.toString());
                    startActivity(intent);
                }
                else
                    Toast.makeText(Feedback.this,"Oops! Please check the form and try again.",Toast.LENGTH_LONG).show();
            }
//                try {
//                    potholes = new JSONObject(getIntent().getStringExtra("jsonpotholes"));
//                    Log.d("POthole data in feedback",potholes.toString());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//


        });
   }

    private boolean isValidateString(String pass)
    {
        if(pass.isEmpty())
            return false;
        return true;
    }
    private boolean isValidatePhone(String pass)
    {

        if(pass.length() != 10)
            return false;
        return true;
    }



    public class FeedbackUpload extends AsyncTask<String,Void,String> {

        // private ProgressDialog pd= new ProgressDialog(PopUp.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(Feedback.this);
            // Set progress dialog title
//        dialog.setTitle("Getting JSON DATA");
            // Set progress dialog message
            dialog.setMessage("Submitting Feedback...");
            dialog.setIndeterminate(false);
            // Show progress dialog
            dialog.show();        }

        @Override
        protected String doInBackground(String... params) {
            URL url;
            HttpURLConnection urlConnection;
            String jsonData;
            jsonData = params[0];
            try {
                url = new URL("http://fixmyroad.in/reviews");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");

                try {
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    //    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                    writer.write(jsonData);
                    writer.flush();
                    writer.close();
                    int responseCode = urlConnection.getResponseCode();
                    String responseText = urlConnection.getResponseMessage();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        Log.d("POST request worked", "HTTPOK");

                    } else
                        Log.d("POST request not worked", responseText);
                    // writeStream(out);
                } finally {
                    urlConnection.disconnect();
                }
            }catch (Exception e){
                e.printStackTrace();
                Crittercism.logHandledException(e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.hide();
            dialog.dismiss();
            Toast.makeText(Feedback.this,"Thanks for Submitting Feedback",Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
