package com.pothole.fixmyroad;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;

/**
 * Created by hp on 9/21/2016.
 */
public class FullImagePothole extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fullimg_pothole);

//        getActionBar().setHomeButtonEnabled(true);
//        getActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle bundle = getIntent().getExtras();

        final String url = bundle.getString("imageUrl");
        final double distance = bundle.getDouble("distance");
        final String email = bundle.getString("emailId");
        final String timestamp = bundle.getString("timestamp");
        ImageView fullImage = (ImageView) findViewById(R.id.fullImage);

        if(url != null && !url.equals("null")) {
            if (url.contains("http")) {
                Log.d("Cheenu http Image url", url);

                Glide.with(this)
                        .load(url)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(fullImage);

            } else {
                Log.d("Cheenu Image local", url);
//                        Picasso.with(MapsActivity.this)
//                                .load(new File(url))
//                                .resize(400, 400)
//                                .error(R.drawable.ic_photo_filter_black_18dp)
//                                .into(holder.Image);
                Glide.with(this)
                        .load(new File(url))
                        .into(fullImage);
            }
        }
        else
        {
            Glide.with(this)
                    .load(R.drawable.ic_no_camera_capture_picture_image)
                    .into(fullImage);
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
        }
        fullImage.setScaleType(ImageView.ScaleType.FIT_XY);
        fullImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=  new Intent(FullImagePothole.this, InfoWindow_Marker.class);
                Bundle bundle = new Bundle();
                bundle.putString("imageUrl",url);
                bundle.putDouble("distance", distance);
                bundle.putString("email",email);
                bundle.putString("timestamp",timestamp);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
