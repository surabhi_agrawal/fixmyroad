package com.pothole.fixmyroad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.crittercism.app.Crittercism;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by hp on 7/30/2016.
 */
public class GetJsonFromCreditsTask extends AsyncTask<Void,Void,String> {

    private Activity activity;
    private URL url;
    private ProgressDialog dialog;
    private final static String TAG = GetJsonFromUrlTask.class.getSimpleName();

    public GetJsonFromCreditsTask(Activity activity, URL url) {
        super();
        this.activity = activity;
        this.url =url;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Create a progress dialog
        dialog = new ProgressDialog(activity);
        // Set progress dialog title
//        dialog.setTitle("Getting JSON DATA");
        // Set progress dialog message
        dialog.setMessage("Loading Credits...");
        dialog.setIndeterminate(false);
        // Show progress dialog
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {

        // call load JSON from url method
        return loadJSON(this.url).toString();
    }

    @Override
    protected void onPostExecute(String result) {
        ((ListViewActivityCredits) activity).parseJsonResponse(result);
        dialog.dismiss();
        Log.i(TAG, result);
    }

    public JSONObject loadJSON(URL url) {
        // Creating JSON Parser instance
        JSONGetter jParser = new JSONGetter();

        // getting JSON string from URL
        JSONObject json = jParser.getJSONFromUrl(url);

        return json;
    }

    private class JSONGetter {

        private InputStream is = null;
        private JSONObject jObj = null;
        private String json = "";
        URL url;
        HttpURLConnection urlConnection;
        String result;
        JSONArray jArray;
        JSONObject jObject;

        // constructor
        public JSONGetter() {

        }

        public JSONObject getJSONFromUrl(URL url1) {

            try {
                url = url1;
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");

                try {
                    urlConnection.setDoOutput(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferReader.close();
                    result = stringBuilder.toString();
                    try{
                        // jArray = new JSONArray(result);
                        jObject= new JSONObject(result);
                        Log.d("GetConnect Aysnc task:",jObject.toString());
                    }catch (JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Crittercism.logHandledException(e);
                    }
                    return jObject;

                } finally {
                    urlConnection.disconnect();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Crittercism.logHandledException(e);
                return null;
            }

        }
    }

}


