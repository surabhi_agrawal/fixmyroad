package com.pothole.fixmyroad;//package com.anubhav.pothole.helloworld;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.TextView;
//import android.R;
//
////package anubhav.hoolia;
//import android.support.v7.app.AppCompatActivity;
//import com.google.android.gms.auth.api.Auth;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
//import com.google.android.gms.auth.api.signin.GoogleSignInResult;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.SignInButton;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
//import com.google.android.gms.common.api.OptionalPendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;
//
//public class GoogleSignIn extends Activity implements OnConnectionFailedListener, View.OnClickListener {
//    private static final int RC_SIGN_IN = 9001;
//    private static final String TAG = "SignInActivity";
//    GoogleSignInAccount acct;
//    private GoogleApiClient mGoogleApiClient;
//    private ProgressDialog mProgressDialog;
//    private TextView mStatusTextView;
//
//
//    protected void onCreate(Bundle savedInstance) {
//        super.onCreate(savedInstance);
//        setContentView(R.layout.google_sign_in);
//        mStatusTextView = (TextView) findViewById(R.id.status);
//        findViewById(R.id.sign_in_button).setOnClickListener(this);
//        findViewById(R.id.sign_out_button).setOnClickListener(this);
//        findViewById(R.id.sign_out_and_disconnect).setOnClickListener(this);
//        GoogleSignInOptions gso = new Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
//        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
//        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
//        signInButton.setSize(0);
//        signInButton.setScopes(gso.getScopeArray());
//    }
//
//    protected void onStart() {
//        super.onStart();
//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(this.mGoogleApiClient);
//        if (opr.isDone()) {
//            Log.d(TAG, "Got cached sign-in");
//            handleSignInResult((GoogleSignInResult) opr.get());
//            return;
//        }
//        showProgressDialog();
//        opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//            @Override
//            public void onResult(GoogleSignInResult googleSignInResult) {
//                hideProgressDialog();
//                handleSignInResult(googleSignInResult);
//            }
//        });
//    }
//
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGN_IN) {
//            handleSignInResult(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
//        }
//    }
//
//    private void handleSignInResult(GoogleSignInResult result) {
//        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
//        if (result.isSuccess()) {
//            GoogleSignInAccount acct = result.getSignInAccount();
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, new Object[]{acct.getDisplayName()}));
//            updateUI(true);
//            return;
//        }
//        updateUI(false);
//    }
//
//    private void signIn() {
//        startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(this.mGoogleApiClient), RC_SIGN_IN);
//    }
//
//    public void signOut() {
//        Auth.GoogleSignInApi.signOut(this.mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
//            @Override
//            public void onResult(Status status) {
//                // [START_EXCLUDE]
//                updateUI(false);
//                // [END_EXCLUDE]
//            }
//        });
//    }
//
//    private void revokeAccess() {
//        Auth.GoogleSignInApi.revokeAccess(this.mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
//            @Override
//            public void onResult(Status status) {
//                // [START_EXCLUDE]
//                updateUI(false);
//                // [END_EXCLUDE]
//            }
//        });
//    }
//
//    public void onConnectionFailed(ConnectionResult connectionResult) {
//        Log.d(TAG, "onConnectionFailed:" + connectionResult);
//    }
//
//    private void showProgressDialog() {
//        if (mProgressDialog == null) {
//            mProgressDialog = new ProgressDialog(this);
//            mProgressDialog.setMessage(getString(R.string.loading));
//            mProgressDialog.setIndeterminate(true);
//        }
//        mProgressDialog.show();
//    }
//
//    private void hideProgressDialog() {
//        if (mProgressDialog != null && this.mProgressDialog.isShowing()) {
//            mProgressDialog.hide();
//        }
//    }
//
//    private void updateUI(boolean signedIn) {
//        if (signedIn) {
//            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
//            startActivity(new Intent(this, MapsActivity.class));
//            return;
//        }
//        mStatusTextView.setText(R.string.signed_out);
//        findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//        findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
//    }
//
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.sign_in_button:
//                signIn();
//            case R.id.sign_out_button:
//                signOut();
//            case R.id.disconnect_button:
//                revokeAccess();
//            default:
//        }
//    }
//}
