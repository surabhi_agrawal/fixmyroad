package com.pothole.fixmyroad;

import com.crittercism.app.Crittercism;

import java.sql.Date;
import java.text.SimpleDateFormat;

//package anubhav.hoolia;

public class HomeAddress_DB {
    int flag;
    double homeLatitude;
    double homeLongitude;
    int id;
    String timeStamp;

    public HomeAddress_DB(double _homeLatitude, double _homeLongitude, int _flag) {
        this.homeLatitude = _homeLatitude;
        this.homeLongitude = _homeLongitude;
        this.flag = _flag;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int _id) {
        this.id = _id;
    }

    public String getTimestamp() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
            return null;
        }
    }

    public void setTimeStamp(String _timeStamp) {
        this.timeStamp = _timeStamp;
    }

    public double getHomeLatitude() {
        return this.homeLatitude;
    }

    public void setHomeLatitude(double _homeLatitude) {
        this.homeLatitude = _homeLatitude;
    }

    public double getHomeLongitude() {
        return this.homeLongitude;
    }

    public void setHomeLongitude(double _homeLongitude) {
        this.homeLongitude = _homeLongitude;
    }

    public int getHomeFlag() {
        return this.flag;
    }

    public void setHomeFlag(int _flag) {
        this.flag = _flag;
    }
}
