package com.pothole.fixmyroad;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hp on 9/16/2016.
 */
public class InfoWindow_Marker extends Activity{
    final String TAG = "InfoWindow_Marker";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infowindow_layout);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;


        getWindow().setLayout((int) (width * .8), (int) (height * .6));

        Bundle bundle = getIntent().getExtras();
        final double distance = bundle.getDouble("distance");
        final String url = bundle.getString("imageUrl");
        ImageView image = (ImageView) findViewById(R.id.marker_icon);
        String email = bundle.getString("email");
        Log.d("split email",email);
        final String[] parts = email.split("@");
        if(parts.length >1)
            email = parts[0];
        Date serverDate = null;
        final String timestamp_temp = bundle.getString("timestamp");
        try {
            serverDate = new Date(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse(timestamp_temp).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
        final String formattedDate = dateFormat.format(serverDate);
//        new SimpleDateFormat("E, dd MMM, h:mm A");
//  String timestamp = new SimpleDateFormat("dd-MMM, h:mm a").format(new Date(timestamp_temp));
        //image.getLayoutParams().width = (int)(width * .7);
        //image.getLayoutParams().height = (int)(height * .6);

//        getActionBar().setTitle(((int) distance) + "kms away");
//        getWindow().setTitle(((int) distance) + "kms away");
        // conveting km to meters and comparing the if the meters is below 1000 or equal to 1000 or greater than 1000
        double meters = distance * 1000;
        TextView textView = (TextView) findViewById(R.id.marker_label);
        TextView textViewTimestamp = (TextView) findViewById(R.id.tv_timestamp);
        if(meters >1000)
        {
            textView.setText(((int) distance) + " kms away");
        }
        else if(meters  == 1000)
        {
            textView.setText(((int) distance) + " km away");
        }
        else
        {
            textView.setText( (int) meters + " meters away");
        }

        TextView textView1 = (TextView) findViewById(R.id.reported);
        textView1.setText(email);
        textViewTimestamp.setText(formattedDate);
        Log.d(TAG,"Cheenu url:"+ url);
        Log.d(TAG, "Cheenu distance:"+distance);
        if(url != null && !url.equals("null")) {
            if (url.contains("http")) {
                Log.d(TAG,"Cheenu http Image url"+ url);
                Glide.with(this)
                                .load(url)
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);

                                    @Override
                                    public boolean onException(Exception e, String model,
                                                               Target<GlideDrawable> target, boolean isFirstResource) {
                                        progressBar.setVisibility(View.GONE);
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        progressBar.setVisibility(View.GONE);
                                        return false;
                                    }
                                })
                                .into(image);

            } else {
                Log.d("Cheenu Image local", url);
//                        Picasso.with(MapsActivity.this)
//                                .load(new File(url))
//                                .resize(400, 400)
//                                .error(R.drawable.ic_photo_filter_black_18dp)
//                                .into(holder.Image);
                Glide.with(this)
                        .load(new File(url))
                        .into(image);
            }
        }
        else
        {
            Glide.with(this)
                    .load(R.drawable.ic_no_camera_capture_picture_image)
                    .into(image);

            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
            progressBar.setVisibility(View.GONE);
        }
        image.setScaleType(ImageView.ScaleType.FIT_XY);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(InfoWindow_Marker.this,FullImagePothole.class);

                Bundle bundle = new Bundle();
                bundle.putString("imageUrl",url);
                bundle.putDouble("distance", distance);
                bundle.putString("emailId", parts[0]);
                bundle.putString("timestamp",timestamp_temp);

                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

//        holder.Distance.setText(new DecimalFormat("##.#").format(Double.valueOf(distance)) + " kms away");
    }


}

