package com.pothole.fixmyroad;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.crittercism.app.Crittercism;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by hp on 7/29/2016.
 */
public class ListViewActivity extends Activity {
    private ListView listview;
    private ArrayList<RowItem_History> potholes;
    private ArrayAdapter<RowItem_History> adapter;
    String possibleEmail;
    private final static String TAG = ListViewActivity.class.getSimpleName();
    DatabaseHandler dbHistory = new DatabaseHandler(this);


    URL url;
 //  public URL url= new URL("http://shole.herokuapp.com/getUserHistory.json?email_id=agrawalsurabhi89@gmail.com");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        // enable ActionBar app icon to behave as action to toggle nav drawer
//        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.ic_arrow_back_white_24dp);
        getActionBar().setHomeButtonEnabled(true);

        // Checking if internet and GPS is on or not if not it will ask you to Enable it
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.setMessage(this.getResources().getString(R.string.network_not_enabled));
            alert.setPositiveButton(this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            alert.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            alert.show();
        }

        try{
            int count = dbHistory.getCountHistory();
//            boolean isCacheExpired = false;
//            if (count > 0)
//            {
//                Date lastDate = dbHistory.getHistConfigDate();
//                long totalElapsedSeconds = DifferenceDate(lastDate);
//                isCacheExpired = totalElapsedSeconds > MapsActivity.CACHE_EXPIRY_HISTORY;
//            }

            if(count == 0)
            {
                try{

                    int countDeleted = dbHistory.deleteAllHistory();
                    Log.d("Deleted rows",countDeleted + "");
                    // reading email id from Device
                    Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                    Pattern phonePattern = Patterns.PHONE;
                    Account[] accounts = AccountManager.get(this).getAccounts();
                    // PERSMISSION ALERT: Account requires permission <uses-permission android:name="android.permission.GET_ACCOUNTS" />
                    Log.d("possible",accounts.toString());
                    for (Account account : accounts) {
                        Log.d("possible in for",account.toString());
                        if (emailPattern.matcher(account.name).matches()) {
                            possibleEmail = account.name;
                            Log.d("possible email",possibleEmail);
                            break;
                        }

                    }
                    url= new URL("http://www.fixmyroad.in/getUserHistory.json?email_id=" + possibleEmail);
                    Log.d("possible",url.toString());
                    //more code goes here
                }catch(MalformedURLException ex){
//do exception handling here
                    ex.printStackTrace();
                    Crittercism.logHandledException(ex);
                }
                listview = (ListView) findViewById(R.id.list);
                setListViewAdapter();
                getDataFromInternet();
                count = dbHistory.getCountHistory();
                Conf conf = new Conf(count);
                dbHistory.addHistoryConfig(conf);
            }
            else
            {

                List<RowItem_History> historyList = new ArrayList<RowItem_History>();
                historyList = dbHistory.getAllHistory();

                listview = (ListView) findViewById(R.id.list);
                CustomListViewAdapter customlistviewadapter = new CustomListViewAdapter(this,R.layout.list_history,historyList);
                listview.setAdapter(customlistviewadapter);

            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }

    private void getDataFromInternet() {
        new GetJsonFromUrlTask(this, url).execute();
    }

    private void setListViewAdapter() {
        potholes = new ArrayList<RowItem_History>();
        adapter = new CustomListViewAdapter(this, R.layout.list_history, potholes);
        listview.setAdapter(adapter);
    }

    //parse response data after asynctask finished
    public void parseJsonResponse(String result) {
        Log.i(TAG, result);
        View footerView = null;
        try {
            JSONObject json = new JSONObject(result);
            int potholeCount = json.getInt("user_count");
            if(potholeCount == 0) {
                footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_history, null, false);
                listview.addFooterView(footerView);
            }
            else {

               JSONArray jArray = new JSONArray(json.getString("users"));
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject jObject = jArray.getJSONObject(i);
                    Log.d("json", jObject.toString());
                    RowItem_History pothole = new RowItem_History();
                    if (jObject.isNull("signup_lat") != true &&
                            jObject.isNull("signup_long") != true &&
                            jObject.isNull("email_id") != true) {
                        pothole.setLatitude(jObject.getDouble("signup_lat"));
                        pothole.setLongitude(jObject.getDouble("signup_long"));
                        pothole.setEmail(jObject.getString("email_id"));
                            if (jObject.isNull("image") != true &&
                                jObject.getJSONObject("image").isNull("image") != true &&
                                jObject.getJSONObject("image").getJSONObject("image").isNull("url") != true) {
                            pothole.setImageUrl(jObject.getJSONObject("image").getJSONObject("image").getString("url"));
                            String urlprint = jObject.getJSONObject("image").getJSONObject("image").getString("url");
                            Log.d("Url fetched from Json", urlprint);
                        }
                        Log.d("cheenu:listview", jObject.getString("created_at"));
                        pothole.setTimeStamp(jObject.getString("created_at"));
                        pothole.setAddress(jObject.getString("address"));
                        pothole.setIsSync(1);
                        potholes.add(pothole);
                        dbHistory.addHistoryPothole(pothole); // adding pothole data into local database/cache
                    }
                }

                ArrayList<RowItem_History> list = new ArrayList<RowItem_History>();
                RowItem_History potholeIsSync = new RowItem_History();
                list = dbHistory.getAllLocalPothole();

                for(RowItem_History ph: list){
                    potholeIsSync.setTimeStamp(ph.getTimeStamp());
                    potholeIsSync.setLatitude(ph.getLatitude());
                    potholeIsSync.setLongitude(ph.getLongitude());
                    potholeIsSync.setImageUrl(ph.getImageUrl());
                    potholeIsSync.setEmail(ph.getEmail());
                    potholeIsSync.setAddress("");
                    potholes.add(potholeIsSync);
                    dbHistory.addHistoryPothole(potholeIsSync);
                }

            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    public long DifferenceDate(Date lastDate)
    {
        long diffMinutes = 0,diffHours=0,diffDays =0;
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date currentDate=new Date(System.currentTimeMillis());
        //milliseconds
        long different = currentDate.getTime() - lastDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

        return elapsedSeconds;
    }

}
