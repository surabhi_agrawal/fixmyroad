package com.pothole.fixmyroad;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.crittercism.app.Crittercism;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hp on 7/30/2016.
 */
public class ListViewActivityCredits extends Activity{

    private ListView listview;
    private ArrayList<RowItem_Credits> potholesCredits;
    private ArrayAdapter<RowItem_Credits> adapter;

    private final static String TAG = ListViewActivityCredits.class.getSimpleName();
    DatabaseHandler dbCredits = new DatabaseHandler(this);

    URL url;
    //  public URL url= new URL("http://shole.herokuapp.com/getUserHistory.json?email_id=agrawalsurabhi89@gmail.com");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credits);


        // enable ActionBar app icon to behave as action to toggle nav drawer
//        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(R.drawable.ic_arrow_back_white_24dp);
        getActionBar().setHomeButtonEnabled(true);

        // Checking if internet and GPS is on or not if not it will ask you to Enable it
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.setMessage(this.getResources().getString(R.string.network_not_enabled));
            alert.setPositiveButton(this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            alert.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            alert.show();
        }

        int count = dbCredits.getCountCredits();
        try {
            if(count == 0 || dbCredits.getCreditConfigDate().compareTo(new Date(System.currentTimeMillis())) > MapsActivity.CACHE_EXPIRY_CREDITS)
            {
                try{

                    url= new URL("http://www.fixmyroad.in/getCredits.json");

                    //more code goes here
                }catch(MalformedURLException ex){
                    Crittercism.logHandledException(ex);
                    //do exception handling here
                }
                listview = (ListView) findViewById(R.id.listCredits);
                setListViewAdapter();
                getDataFromInternet();
                count = dbCredits.getCountCredits();
                Conf conf = new Conf(count);
                dbCredits.addCreditsConfig(conf);
            }

            else
            {
                List<RowItem_Credits> creditList = new ArrayList<RowItem_Credits>();
                creditList = dbCredits.getAllCredit();
                listview = (ListView) findViewById(R.id.listCredits);
                CustomListViewAdapterCredits customListViewAdapterCredits= new CustomListViewAdapterCredits(this,R.layout.list_credits,creditList);
                listview.setAdapter(customListViewAdapterCredits);

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void getDataFromInternet() {
        new GetJsonFromCreditsTask(this, url).execute();
    }

    private void setListViewAdapter() {
        potholesCredits = new ArrayList<RowItem_Credits>();
        adapter = new CustomListViewAdapterCredits(this, R.layout.list_credits, potholesCredits);
        listview.setAdapter(adapter);
    }

    //parse response data after asynctask finished
    public void parseJsonResponse(String result) {
        Log.i(TAG, result);
        try {
            JSONObject json = new JSONObject(result);

            JSONArray jArray = new JSONArray(json.getString("credit"));
            for (int i = 0; i < jArray.length(); i++) {

                JSONObject jObject = jArray.getJSONObject(i);
                Log.d("json",jObject.toString());
                RowItem_Credits pothole = new RowItem_Credits();
                if (jObject.isNull("count") != true &&
                        jObject.isNull("name") != true ) {
                    pothole.setCount_potholes(jObject.getInt("count"));
                    pothole.setEmail(jObject.getString("name"));

                    potholesCredits.add(pothole);
                    dbCredits.addCredits(pothole);
                }
            }

            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
