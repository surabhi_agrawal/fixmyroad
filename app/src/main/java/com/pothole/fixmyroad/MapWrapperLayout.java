package com.pothole.fixmyroad;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

//package anubhav.hoolia;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class MapWrapperLayout extends FrameLayout {
    private OnDragListener mOnDragListener;

    public interface OnDragListener {
        void onDrag(MotionEvent motionEvent);

        void onNavigationDrawerItemSelected(int i);
    }

    public MapWrapperLayout(Context context) {
        super(context);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.mOnDragListener != null) {
            this.mOnDragListener.onDrag(ev);
        }
        return super.dispatchTouchEvent(ev);
    }

    public void setOnDragListener(OnDragListener mOnDragListener) {
        this.mOnDragListener = mOnDragListener;
    }
}
