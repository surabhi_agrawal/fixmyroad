package com.pothole.fixmyroad;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.provider.Settings;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.crittercism.app.Crittercism;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.GET_ACCOUNTS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MapsActivity extends FragmentActivity implements GpsStatus.Listener,LocationListener, OnMapReadyCallback, SensorEventListener, GoogleMap.OnMyLocationButtonClickListener{

    public static int THRESHOLD;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private ImageView mapMarker;
    private int imageHeight = -1;
    private int centerX = -1;
    private int centerY = -1;
    LatLng centerLatLng;
    private float zoomLevel = 14;
    private SQLiteDatabase sqlDB;
    double latitude, longitude;
    LocationManager locationManager;
    DatabaseHandler db = new DatabaseHandler(this);
    Potholes_DB pothole;
    private Sensor accelerometer;
    private SensorManager sm;
    private ProgressDialog dialog;
    private long lastTime = 0;
    private float lastX, lastY, lastZ;
    private static final int PERMISSION_REQUEST_CODE = 1;
   // private static final int THRESHOLD = 2000; //used to see whether a shake gesture has been detected or not.
    private DrawerLayout mDrawerLayout; // for navigation - drawer in Activity
    private ListView mDrawerList; // for navigation bar - lists in Navigation
    private CharSequence mDrawerTitle; // for getting Drawer Title
    private CharSequence mTitle;     // for getting title
    private String[] mListTitles;   // for getting list title
    private ActionBarDrawerToggle mDrawerToggle;
    private GoogleApiClient mGoogleApiClient;
    private HashMap<Marker, MyMarker> mMarkersHashMap;
    private List<HashMap<String, String>> mList; // Created for adding icons in Listview items
    private static final String CRITTERCISM_APP_ID = "c2b17ab702a34edc9820989c973dfef900555300";
    final private String MENUS = "menus"; // Created for adding icons in Listview items
    final private String FLAG = "flag"; // Created for adding icons in Listview items
    int[] mFlags = new int[]{R.drawable.ic_lightbulb_outline_white_18dp,
                             R.drawable.ic_timeline_white_18dp,
                             R.drawable.ic_people_white_18dp,
                             R.drawable.ic_rate_review_white_18dp,
                             R.drawable.ic_share_white_18dp,
                             R.drawable.ic_feedback_white_18dp};

    private SimpleAdapter mAdapter;
    JSONObject potholes;
    int pothole_id;
    String str_image_path;
    public static final int POTHOLE_SELECT = 1;
    JSONObject jobject;
    public static String APP_INVITE_SMS;
    public static int CACHE_EXPIRY_HISTORY;
    public static int CACHE_EXPIRY_ABOUT_SCR;
    public static int CACHE_EXPIRY_POTHOLES;
    public static int CACHE_EXPIRY_CREDITS;
    public static int VECHILE_SPEED_THRESHOLD;
    private ProgressBar progressBar;
    double mySpeed;


    private PendingIntent pendingIntent;
    GregorianCalendar calendar;

    private Handler handler = new Handler() {
        public void handleMessage(Message message) {
            Object path = message.obj;
            if (message.arg1 == RESULT_OK || message.arg1 == Activity.RESULT_OK) {
//                Toast.makeText(MapsActivity.this,
//                        "Downloaded", Toast.LENGTH_LONG)
//                        .show();
                Log.d("Service Worked","Cheenu");
                updateDatabasePothole();

            } else {
                Log.d("Service not worked",message.toString());
//                Toast.makeText(MapsActivity.this, "Download failed.",
//                        Toast.LENGTH_LONG).show();
            }

        }
    };
    AlarmManager alarmManager;

    private boolean flag = false;
    // private final static String TAG = MapsActivity.class.getSimpleName();

   // String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    // Called when activity  get first created
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_maps);
        if (!checkPermission()) {
            requestPermission();
        } else {
//            Toast.makeText(MapsActivity.this, "Permission already granted.", Toast.LENGTH_LONG).show();
        }

        new UpdateRunnable(this, new Handler(),1000).start();
        JSONObject finalObj = new JSONObject();

//        ArrayList<Potholes_DB> allPotholes = (ArrayList<Potholes_DB>) db.getPotholesIsSync();

//        JSONArray jsArray = new JSONArray();
//
//        String ba1;
//        JSONObject jGroup =new JSONObject();
//        for(int i = 0 ; i < allPotholes.size();i++)
//        {
//            try {
//                jGroup.put("device_id",allPotholes.get(i).getDeviceid());
//                jGroup.put("email_id",allPotholes.get(i).getEmailId());
//                jGroup.put("signup_lat",allPotholes.get(i).getlatitude());
//                jGroup.put("signup_long",allPotholes.get(i).getlongitude());
//                jGroup.put("pathofimage",allPotholes.get(i).getPathofImage());
//                Bitmap bm = BitmapFactory.decodeFile(allPotholes.get(i).getPathofImage());
//                Log.d("Image File size", ""+(allPotholes.get(i).getPathofImage()).length());
//                ByteArrayOutputStream bao = new ByteArrayOutputStream();
//                bm.compress(Bitmap.CompressFormat.JPEG,80, bao); // Changed from 50 to 100 for the checking the quality of images sent to server
//                byte[] by = bao.toByteArray();
//                ba1 = Base64.encodeToString(by, Base64.DEFAULT);
//                jGroup.put("image",ba1);
//                jGroup.put("timestamp",allPotholes.get(i).getTimestamp());
//                jGroup.put("comments",allPotholes.get(i).getComments());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


            //      finalObj.put("is_bulk_upload",true);
//                finalObj.put("user",jArray);
//                finalObj.put("user",jGroup);
//            jsArray.put(jGroup);
//            Log.d("FinalJSON",jsArray.toString());
//
//        }
        // One time for sending data to server as data is important and real 20/10/2016
//        db.SetIsSyncToZero();
        calendar = (GregorianCalendar) Calendar.getInstance();
        Intent myIntent = new Intent(MapsActivity.this, PotholeService.class);
        Messenger messenger = new Messenger(handler);
        myIntent.putExtra("MESSENGER", messenger);
//        myIntent.setData(Uri
//                .parse("http://10.0.2.2/message/message.json"));// nawinsandroidtutorial.site90.com/message
//        myIntent.putExtra("urlpath",
//                "http://10.0.2.2/message/message.json");
        Bundle bundle = new Bundle();
//        bundle.putString("parentObj",jsArray.toString());
//        myIntent.putExtra("parentObj",finalObj.toString());
//        bundle.putString("parentObj","IntentService");

        myIntent.putExtras(bundle);
        pendingIntent = PendingIntent.getService(this, 0,
                myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
                100000, pendingIntent);
        if (flag) {
            flag = false;
//            b1.setText("Start update");
            alarmManager.cancel(pendingIntent);
        }
        else {
            flag = true;
//            b1.setText("Stop update");
            alarmManager.setRepeating(AlarmManager.RTC,
                    calendar.getTimeInMillis(), 100000, pendingIntent);
        }

        // /Code for Checking permission and Granting

        String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        // Code Starts for checking Internet and GPS Enabled on Device
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
//        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
//                            0, locationlistener);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }

        try {
            jobject = new GetInit().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
        } catch (ExecutionException e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
        }

        try
        {
            if (jobject != null) {
                THRESHOLD = jobject.getInt("threshold_acceleratometer");
                APP_INVITE_SMS = jobject.getString("app_invite_sms");
                CACHE_EXPIRY_HISTORY = jobject.getInt("cache_expiry_history");
                CACHE_EXPIRY_ABOUT_SCR = jobject.getInt("cache_expiry_about_scr");
                CACHE_EXPIRY_POTHOLES = jobject.getInt("cache_expiry_potholes");
                CACHE_EXPIRY_CREDITS = jobject.getInt("cache_expiry_credits");
                VECHILE_SPEED_THRESHOLD = jobject.getInt("threshold_vehicle_speed");
            }else
            {
                THRESHOLD = 100;
                APP_INVITE_SMS = "Fed up with potholes in your city? Report potholes in your locality " +
                        "with a finger touch. Download FixMyRoad http://bit.ly/FixMyRoad now.";
                CACHE_EXPIRY_HISTORY = 3600;
                CACHE_EXPIRY_ABOUT_SCR = 3600;
                CACHE_EXPIRY_POTHOLES = 3600;
                CACHE_EXPIRY_CREDITS = 5;
                VECHILE_SPEED_THRESHOLD = 5;
            }

        }catch(JSONException e)
        {
            THRESHOLD = 100;
            APP_INVITE_SMS = "Fed up with potholes in your city? Report potholes in your locality " +
                    "with a finger touch. Download FixMyRoad http://bit.ly/FixMyRoad now.";
            CACHE_EXPIRY_HISTORY = 3600;
            CACHE_EXPIRY_ABOUT_SCR = 3600;
            CACHE_EXPIRY_POTHOLES = 3600;
            CACHE_EXPIRY_CREDITS = 5;
            VECHILE_SPEED_THRESHOLD = 5;

            e.printStackTrace();
            Crittercism.logHandledException(e);
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present

            alert.setMessage(this.getResources().getString(R.string.network_not_enabled));
            alert.setPositiveButton(this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub.
                    Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            alert.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub


                }
            });
            alert.show();
        }


        // Code Ends for Checking Internet and GPS Enabled on Device
        Crittercism.initialize(getApplicationContext(), CRITTERCISM_APP_ID); // apteligent code for crash report system integration 27 July 2016, 1:30 PM
        setContentView(R.layout.activity_maps);
        this.mMarkersHashMap = new HashMap<Marker, MyMarker>();

        mTitle = mDrawerTitle = getTitle();
        mListTitles = getResources().getStringArray(R.array.maps_array);
        mList = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < 6; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put(MENUS, mListTitles[i]);
            hm.put(FLAG, Integer.toString(mFlags[i]));
            mList.add(hm);
        }
        // Keys used in Hashmap
        String[] from = {FLAG, MENUS};

        // Ids of views in listview_layout
        int[] to = {R.id.flag, R.id.textmenus};

        // code for getting ListView and Navigation Bar in Main Activity
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mAdapter = new SimpleAdapter(this, mList, R.layout.drawer_list, from, to);
        mDrawerList.setAdapter(mAdapter);
//        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
//                R.layout.drawer_list,mListTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        getActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

//        if (savedInstanceState == null) {
//            selectItem(0);
//        }
// Drawer code end here


        MapFragment mapFragment =
                (MapFragment) this.getFragmentManager().findFragmentById(R.id.map);
//        View myLocationButton = mapFragment.getView().findViewById(0x2);



        mapFragment.getMapAsync(this);

        //for positioning Location Button on Map
        View myLocationButton = ((View) mapFragment.getView()
                .findViewById(Integer.parseInt("1"))
                .getParent()).findViewById(Integer.parseInt("2"));

        if (myLocationButton != null
                && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            // location button is inside of RelativeLayout
            RelativeLayout.LayoutParams params
                    = (RelativeLayout.LayoutParams) myLocationButton.getLayoutParams();

            // Align it to - parent BOTTOM|LEFT

            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
//            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
            // Update margins, set to 10dp
//            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
//                    getResources().getDisplayMetrics());
//            params.setMargins(left, top, right, bottom);
//            params.setMargins(0,100, 300, 200);

            myLocationButton.setLayoutParams(params);
        }

        // for reading accelerometer sensors
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            //sucess we have an accelerometer
            accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            // Fail we dont have an accelerometer
            Log.d("Accelerometer SensrMgr ", "Fail we dont have an accelerometer");
        }

        mapMarker = (ImageView) findViewById(R.id.drop_map_marker_icon_view);

        ImageView icon = new ImageView(this); // Create an icon
        icon.setImageResource(R.drawable.ic_add_a_photo_black_36dp);

        //  setUpMapIfNeeded();

        FloatingActionButton actionButton = new FloatingActionButton.Builder(this)
                .setContentView(icon)
                .build();

//        actionButton.setPosition( FloatingActionButton.POSITION_LEFT_CENTER,);

//        RelativeLayout.LayoutParams floatingButtonParams
//                = (RelativeLayout.LayoutParams) actionButton.getLayoutParams();

        // Align it to - parent BOTTOM|LEFT
//        floatingButtonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,1);
//        floatingButtonParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT,1);
//            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
//            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);

//        actionButton.setLayoutParams(floatingButtonParams);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, PopUp.class);

                //getting new location
//                DisplayMetrics dm = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(dm);
////
//                int width = dm.widthPixels;
//                int height = dm.heightPixels;
////
//                imageHeight = mapMarker.getHeight();
//
//                centerX = width / 2;
//                centerY = (height / 2) /*+ (imageHeight/2)*/;
//
//
//
//
//                Log.d("tag", "onDrag(MotionEvent motionEvent)");
//                Projection projection = (mMap != null && mMap
//                        .getProjection() != null) ? mMap.getProjection()
//                        : null;
//                //
//                if (projection != null) {
//                    centerLatLng = projection.fromScreenLocation(new Point(
//                            centerX, centerY));
//
//                    Log.d("tag", "centerLatLng:" + centerLatLng.toString());
//
//                    // mMap.addMarker(new MarkerOptions().position(centerLatLng).title("Bad Road"));
//                    zoomLevel = mMap.getCameraPosition().zoom;
//                } else {
//                    Log.d("tag", "projection == null");
//                }
                centerLatLng = mMap.getCameraPosition().target;
                LatLng latlongmarker = new LatLng(centerLatLng.latitude, centerLatLng.longitude);
//                LatLng latlongmarker = mMap.getCameraPosition().target;
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlongmarker, 13));
//                mMap.addMarker(new MarkerOptions()
//                        .position(latlongmarker)
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));
//               // double latMap = centerLatLng.latitude;
//                //double longMap = centerLatLng.longitude;
                String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

                Bundle bundle = new Bundle();
                bundle.putDouble("signup_lat", centerLatLng.latitude);
                bundle.putDouble("signup_long", centerLatLng.longitude);
                bundle.putString("Device_id", id);
                intent.putExtras(bundle);

                Log.d("Cheenu","MapsActivity: deviceid:" + id);

                startActivityForResult(intent, POTHOLE_SELECT); //changed for doing testing of Database entry in Pothole table

////                Potholes_DB addPothole = new Potholes_DB(latMap,longMap,null,null,id);
////                // for adding pothole data into database
////                //db.addPothole(new Potholes_DB(latMap,longMap,null,null,id));
////                db.addPothole(addPothole);
////                JSONObject parentObj = new JSONObject();
////                JSONObject childObj = new JSONObject();
////                try{
////                    childObj.put("signup_lat",addPothole.getlatitude());
////                    childObj.put("signup_long",addPothole.getlongitude());
////                    parentObj.put("user",childObj);
////
////                }catch (Exception e){
////
////                }
////                //connecting Post request to insert pothole data into Server DB
////                PostConnects m = new PostConnects();
////                m.execute(String.valueOf(parentObj));
////
////                // for logging purpose Testing
////               Log.d("Records","Lat Long" + centerLatLng);
//            }
//        });
            }
        });
    }

    // Method to manually check connection status

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data)
    {
        String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//        Log.d("Cheenu", "onActivityResult::resultcode:"+resultCode);
//        Log.d("Cheenu", "onActivityResult::requestcode:"+requestCode);
//        Log.d("Cheenu", "onActivityResult::POTHOLE_SELECT:"+POTHOLE_SELECT);
//        Log.d("Cheenu", "onActivityResult::RESULT_OK:"+RESULT_OK);
//        Log.d("Cheenu", "onActivityResult::Data.deviceid:"+data.getExtras().getString("emailid"));

        switch(requestCode)
        {
            case POTHOLE_SELECT:
                if(resultCode == RESULT_OK)
                {
                    Bundle bundle = data.getExtras();
                    String email = bundle.getString("emailid");
                    double lat = bundle.getDouble("signup_lat");
                    double lng = bundle.getDouble("signup_long");
                    String imagePath = bundle.getString("image");
                    Log.d("IMGE PATH IN ACTIVITY",imagePath);
                    String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date(System.currentTimeMillis()));
                    Log.d("TIMESTAMP INSERTING",timeStamp);
                    int id = bundle.getInt("user_id");
                    Potholes_DB pothole = new Potholes_DB(timeStamp,lat, lng, imagePath,"",device_id,email,0);
                    db.addPothole(pothole);
//                    String timestamp = new SimpleDateFormat("dd-MMM-yyyy hh:mm").format(new java.sql.Date(System.currentTimeMillis()));
//                    Date timestamp = new java.sql.Date(System.currentTimeMillis());
                    RowItem_History historyPothole = new RowItem_History(lat,lng,imagePath,email,timeStamp,0);
                    db.addHistoryPothole(historyPothole);
                    MyMarker localMyMarker = new MyMarker(
                            id,
                            imagePath,
                            lat,
                            lng,email,timeStamp);
                    Marker localMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));

                    mMarkersHashMap.put(localMarker, localMyMarker);
                    break;
                }
        }
    }
    public boolean checkPermission() {

        int perm_location = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int perm_camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int perm_write = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
//        int perm_accounts = ContextCompat.checkSelfPermission(getApplicationContext(), GET_ACCOUNTS);

        return (perm_location == PackageManager.PERMISSION_GRANTED)
                && (perm_camera == PackageManager.PERMISSION_GRANTED)
                && (perm_write == PackageManager.PERMISSION_GRANTED);
//                && (perm_accounts == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, CAMERA, WRITE_EXTERNAL_STORAGE,GET_ACCOUNTS}, PERMISSION_REQUEST_CODE);

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MapsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean accountsAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted && cameraAccepted && storageAccepted && accountsAccepted) {
//                        Toast.makeText(MapsActivity.this, "Permission Granted, Now you can access location data/camera/storage/accounts", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(MapsActivity.this, "Your app might not work without permissions. Please restart your app.", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION,WRITE_EXTERNAL_STORAGE, CAMERA, GET_ACCOUNTS},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        }
                                );
                                return;
                            }
                        }
                    }
                }

                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    // For Sensor accerlometer reading and Notifying user about the same
    @Override
    public void onSensorChanged(SensorEvent event) {

        if (!checkPermission()) {
            requestPermission();
        } else {
//            Toast.makeText(MapsActivity.this, "Permission already granted.", Toast.LENGTH_LONG).show();

            Sensor sensor = event.sensor;
            String possibleEmail = null;
            double lat = 0, lang = 0;
            if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                long currentTime = System.currentTimeMillis();
                if ((currentTime - lastTime) > 100) {
                    long diffTime = (currentTime - lastTime);
                    lastTime = currentTime;
                    float acce_speed = Math.abs(x + y + z - lastX - lastY - lastZ) / diffTime * 10000;
                    LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                    LocationListener locationListener = new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            location.getLatitude();
                            mySpeed = location.getSpeed();
                            mySpeed = mySpeed * 3.6;
//                        Log.d("Speed",mySpeed +"");
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {

                        }

                        @Override
                        public void onProviderEnabled(String provider) {

                        }

                        @Override
                        public void onProviderDisabled(String provider) {

                        }
                    };


                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                            0, locationListener);

                    if ((acce_speed > THRESHOLD) && (mySpeed > VECHILE_SPEED_THRESHOLD)) {
                        // call the Post service to store the pothole autodetected by device

                        Intent intent = new Intent(this, MapsActivity.class);
                        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
                        // build Notification
                        //add action re use the same intent
//                    Notification n = new Notification.Builder(this).setContentTitle("FixMyRoad").setContentInfo("Pothole Detected,Please Mark it!").setSmallIcon(R.drawable.road_filled_50).build();
//
//                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                    notificationManager.notify(0, n);

                        GPSTracker gps = new GPSTracker(MapsActivity.this);
                        if (gps.canGetLocation) {
                            gps.getLocation();

                            lat = gps.getLatitude();
                            lang = gps.getLongitude();
                        }


                        // reading email id from Device
                        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+

                        Account[] accounts = AccountManager.get(this).getAccounts();
                        // PERSMISSION ALERT: Account requires permission <uses-permission android:name="android.permission.GET_ACCOUNTS" />

                        for (Account account : accounts) {
                            Log.d("account", account.toString());
                            if (emailPattern.matcher(account.name).matches()) {
                                possibleEmail = account.name;
                                break; // for reading only one email id of the user from accounts (changes after testing as History is not coming for several users)
                            }
                        }

                        String manufacturer = Build.MANUFACTURER;
                        String model = Build.MODEL;

                        int version = Build.VERSION.SDK_INT;
                        String versionRelease = Build.VERSION.RELEASE;
                        JSONObject parentObj = new JSONObject();
                        JSONObject childObj = new JSONObject();

                        try {
                            childObj.put("acceleratometer_x", x);
                            childObj.put("acceleratometer_y", y);
                            childObj.put("acceleratometer_z", z);
                            childObj.put("threshold", THRESHOLD);
                            childObj.put("accel_speed", acce_speed);
                            childObj.put("client", "android");
                            childObj.put("source", "auto");
                            childObj.put("OS_VERSION", version);
                            childObj.put("email_id", possibleEmail);
                            childObj.put("signup_lat", lat);
                            childObj.put("signup_long", lang);
                            // childObj.put("Device_id",deviceID);
                            childObj.put("PRODUCT_MODEL", model);
                            childObj.put("vehicle_speed", mySpeed);
                            childObj.put("PRODUCT_MANUFACTURER", manufacturer);
                            parentObj.put("user", childObj);
                            Log.d("Json Object", parentObj.toString());
                        } catch (Exception e) {
                            Toast.makeText(this, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            Crittercism.logHandledException(e);
                        }
                        try {
                            PopUp popup = new PopUp();
                            PopUp.UploadToServerWithOutProgress upload = popup.new UploadToServerWithOutProgress();
                            upload.execute(String.valueOf(parentObj));
                        } catch (Exception e) {
                            e.printStackTrace();
                            Crittercism.logHandledException(e);
                        }


                    }

                    lastX = x;
                    lastY = y;
                    lastZ = z;
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("Surabhi", "onMapReady Invoked");
        mMap = googleMap;
        if (mMap == null) {
            Log.d("Surabhi", "onMapReady:mMap=NULL");

            // check if google play service in the device is not available or out-dated.
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        }
            else
            Log.d("Surabhi", "onMapReady:mMap!=NULL");
        LatLng loc1 = enableMyLocation();
//        LatLng loc1 = new LatLng(12.9667, 77.5667);
//        mMap.setMyLocationEnabled(true);
//        LatLng loc1= new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc1, 13));
//        mMap.addMarker(new MarkerOptions()
//                .position(loc1)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));
        mMap.setOnMyLocationButtonClickListener(MapsActivity.this);

        // Reading all potholes
        Log.d("Reading: ", "Reading  and Marking all potholes on map");
        // commented as of now to test the get service - 25th Feb 2016
        //    List<Potholes_DB> pothole = db.getAllPotholes();
        //
        //    for (Potholes_DB ph : pothole) {
        //        //new LatLng()
        //        LatLng ltlg = new LatLng(ph.getlatitude(),ph.getlongitude());
        //        mMap.addMarker(new MarkerOptions().position(ltlg).title("Bad Road"));
        //    }


        // m.execute();
// working code for reading potholes and marking it on map from Aysnc task starts here

//        try {
//            potholes = new JSONObject(getIntent().getStringExtra("jsonpotholes")); // getting data from Splash Screen for all potholes from server
//            //JSONObject potholes = new GetConnects().execute().get(); // commented this line as the potholes are reading in Splash screen and data is passed from SPlash to this activity
//            Log.d("Get Method called", "all potholes");
//            Log.d("JSON of potholes", "" + potholes);
//            int countSpotholes = potholes.getInt("count_spotholes");
//            JSONArray allPotholes = potholes.getJSONArray("users");
//
//            for (int i = 0; i < allPotholes.length(); i++) {
//                JSONObject finalObject = allPotholes.getJSONObject(i);
//                if (finalObject.isNull("signup_lat") != true &&
//                        finalObject.isNull("signup_long") != true) {
//                    double lat = finalObject.getDouble("signup_lat");
//                    double lang = finalObject.getDouble("signup_long");
//                    int pothole_id = finalObject.getInt("id");
//                    String str_image_path = finalObject.getJSONObject("image").getJSONObject("image").getString("url");
//
//                    LatLng latlang = new LatLng(lat, lang);
//
//                    MyMarker localMyMarker = new MyMarker(
//                            pothole_id,
//                            str_image_path,
//                            lat,
//                            lang);
//                    Marker localMarker = mMap.addMarker(new MarkerOptions().position(latlang).title("Pothole").icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));
//                    mMarkersHashMap.put(localMarker, localMyMarker);
//
//
//                    mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
////                    GoogleMap googleMap2 = mMap;
////                    MarkerInfoWindowAdapter localMarkerInfoWindowAdapter = new MarkerInfoWindowAdapter();
////                    googleMap2.setInfoWindowAdapter(localMarkerInfoWindowAdapter);
//                }
//            }
//            Toast.makeText(MapsActivity.this, "Report potholes and water logging issues. " + countSpotholes + " issues reported.", Toast.LENGTH_LONG).show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
// working code for reading potholes and marking it on map from Aysnc task ends here

// trying code for implementing cache through DatabaseHandler

        try {
            int countSpotholes = 0;
            boolean isCacheExpired = false;
            double lat = 0, lang = 0;
//            int dbCount = db.getConfigCount();
            String emailId =null;
            String timeStamp = null;
            String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            if (db.getCountPotholeIsSync() == 0) {
                //insert into Config table  - total count of potholes and timestamp

                JSONObject potholes = null;

                try {
                    GPSTracker gps = new GPSTracker(MapsActivity.this);
                    if(gps.canGetLocation)
                    {
                        gps.getLocation();
                        lat= gps.getLatitude();
                        lang = gps.getLongitude();
                    }
                    JSONObject jObject = new JSONObject();
                    jObject.put("curr_lat",lat);
                    jObject.put("curr_long",lang);
                    GetConnects getConnects = new GetConnects();

                    potholes = getConnects.execute(jObject).get();


                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }

                Log.d("Get Method called", "all potholes");
                Log.d("JSON of potholes", "" + potholes);
                try {
                    countSpotholes = potholes.getInt("count_spotholes");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }
                JSONArray allPotholes = null;
                try {
                    allPotholes = potholes.getJSONArray("users");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }

                for (int i = 0; i < allPotholes.length(); i++) {
                    JSONObject finalObject = null;
                    try {
                        finalObject = allPotholes.getJSONObject(i);

                        if (!finalObject.isNull("signup_lat") &&
                                !finalObject.isNull("signup_long") ) {
                            emailId = finalObject.getString("email_id");
                            lat = finalObject.getDouble("signup_lat");
                            lang = finalObject.getDouble("signup_long");

                            pothole_id = finalObject.getInt("id");
                            str_image_path = finalObject.getJSONObject("image").getJSONObject("image").getString("url");
                            timeStamp = finalObject.getString("created_at");
                            LatLng latLng = new LatLng(lat, lang);
                            Log.d("JsonEmail",emailId);
                            MyMarker localMyMarker = new MyMarker(
                                    pothole_id,
                                    str_image_path,
                                    lat,
                                    lang,emailId,timeStamp);
                            Marker localMarker = mMap.addMarker(new MarkerOptions()
                                    .position(latLng)
                                    .title("Pothole")
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));
                            mMarkersHashMap.put(localMarker, localMyMarker);

//                            mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                            mMap.setOnMarkerClickListener(new MarkerClickListener());
                            Potholes_DB pot = new Potholes_DB(timeStamp,lat, lang, str_image_path,
                                    "", device_id,emailId,1);
                            db.addPothole(pot);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crittercism.logHandledException(e);

                    }
                }
                Toast.makeText(MapsActivity.this, countSpotholes + " potholes reported so far. How many by you?", Toast.LENGTH_LONG).show();
                Conf cnf = new Conf(countSpotholes);
                db.addConfig(cnf);  // inserting count
            }

            else {

                Date lastDate = db.getConfigDate();
                long totalElapsedSeconds = DifferenceDate(lastDate);
                isCacheExpired = totalElapsedSeconds > CACHE_EXPIRY_POTHOLES;

                if (isCacheExpired == true) {
                    int countDelete = db.deleteAllPothole();
                    Log.d("CountDeletedROws", countDelete + "");
                    JSONObject potholes = null;
                    try {
                        GPSTracker gps = new GPSTracker(MapsActivity.this);
                        if(gps.canGetLocation)
                        {
                            gps.getLocation();
                            lat= gps.getLatitude();
                            lang = gps.getLongitude();
                        }
                        JSONObject jObject = new JSONObject();
                        jObject.put("curr_lat",lat);
                        jObject.put("curr_long",lang);

                        potholes = new GetConnects().execute(jObject).get();


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Crittercism.logHandledException(e);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        Crittercism.logHandledException(e);
                    }
                    Log.d("Get Method called", "all potholes");
                    Log.d("JSON of potholes", "" + potholes);
                    try {
                        countSpotholes = potholes.getInt("count_spotholes");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crittercism.logHandledException(e);
                    }
                    JSONArray allPotholes = null;
                    try {
                        allPotholes = potholes.getJSONArray("users");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Crittercism.logHandledException(e);
                    }

                    for (int i = 0; i < allPotholes.length(); i++) {
                        JSONObject finalObject = null;
                        try {
                            finalObject = allPotholes.getJSONObject(i);

                            if (finalObject.isNull("signup_lat") != true &&
                                    finalObject.isNull("signup_long") != true) {
                                emailId = finalObject.getString("email_id");
                                lat = finalObject.getDouble("signup_lat");
                                lang = finalObject.getDouble("signup_long");
                                pothole_id = finalObject.getInt("id");
                                str_image_path = finalObject.getJSONObject("image").getJSONObject("image").getString("url");
                                timeStamp = finalObject.getString("created_at");
                                LatLng latLng = new LatLng(lat, lang);
                                MyMarker localMyMarker = new MyMarker(
                                        pothole_id,
                                        str_image_path,
                                        lat,
                                        lang, emailId, timeStamp);
                                Marker localMarker = mMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .title("Pothole")
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.reddot)));
                                mMarkersHashMap.put(localMarker, localMyMarker);

//                            mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                                mMap.setOnMarkerClickListener(new MarkerClickListener());
                                Potholes_DB pot = new Potholes_DB(timeStamp, lat, lang, str_image_path,
                                        "", device_id, emailId, 1);
                                db.addPothole(pot);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Crittercism.logHandledException(e);
                        }
                    }
                    Toast.makeText(MapsActivity.this, "Report potholes and water logging issues. " + countSpotholes + " issues reported.", Toast.LENGTH_LONG).show();
                    Conf cnf = new Conf(countSpotholes);
                    db.addConfig(cnf);  // inserting count
                } else {
                    // Reading all potholes
                    Log.d("Reading: ", "Reading  and Marking all potholes on map");
                    // commented as of now to test the get service - 25th Feb 2016
                    List<Potholes_DB> pothole = db.getAllPotholes();

                    for (Potholes_DB ph : pothole) {
                        //new LatLng()
                        LatLng ltlg = new LatLng(ph.getlatitude(), ph.getlongitude());
                        // String str_img = ph.getPathofImage();
                        // int pothole_db_id = ph.getID();
                        //mMap.addMarker(new MarkerOptions().position(ltlg).title("Bad Road"));
                        MyMarker localMyMarker = new MyMarker(
                                ph.getID(),
                                ph.getPathofImage(),
                                ph.getlatitude(),
                                ph.getlongitude(),
                                ph.getEmailId(),
                                ph.getTimestamp());
                        Marker localMarker = mMap.addMarker(
                                new MarkerOptions()
                                        .position(ltlg)
                                        .title("Pothole")
                                        .icon(BitmapDescriptorFactory
                                                .fromResource(R.drawable.reddot))
                        );
                        Log.d("Email: local db before hashmap", localMyMarker.getEmail());
                        mMarkersHashMap.put(localMarker, localMyMarker);

//                  mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                        mMap.setOnMarkerClickListener(new MarkerClickListener());
                    }
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            Crittercism.logHandledException(e);
        }

        // code ends here for reading and marking pothole through tables
    }

    private LatLng enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            GPSTracker gpsTracker = new GPSTracker(MapsActivity.this);
            return new LatLng(gpsTracker.getLatitude(),gpsTracker.getLongitude());

        }
        return new LatLng(12.9667, 77.5667);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkPermission()) {
            requestPermission();
        } else {
//            Toast.makeText(MapsActivity.this, "Permission already granted.", Toast.LENGTH_LONG).show();
        }
        // setUpMapIfNeeded();
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            if(location != null)
            {
                if(location.hasSpeed())
                {
                    mySpeed = location.getSpeed();
                    Log.d("Speed",mySpeed +"" + location.getAltitude());
                }
            }
            return;
        }
        locationManager.removeUpdates(this);
//
//        //open the map:
//        latitude = location.getLatitude();
//        longitude = location.getLongitude();
        //Text(this, "latitude:" + latitude + " longitude:" + longitude, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        // Toast.makeText(this, "Getting current GPS Location", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onGpsStatusChanged(int event) {

    }


    class GetConnects extends AsyncTask<JSONObject,Void,JSONObject> {
        URL url;
        HttpURLConnection urlConnection;
        String result;
        JSONArray jArray;
        JSONObject jObject;
        String possibleEmail  = null;
        double curr_lat, curr_long;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(JSONObject... params) {

            try {

                JSONObject jsonObject= params[0];

                curr_lat = jsonObject.getDouble("curr_lat");
                curr_long = jsonObject.getDouble("curr_long");
                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+

                Account[] accounts = AccountManager.get(MapsActivity.this).getAccounts();
                // PERSMISSION ALERT: Account requires permission <uses-permission android:name="android.permission.GET_ACCOUNTS" />

                for (Account account : accounts) {
                    Log.d("account", account.toString());
                    if (emailPattern.matcher(account.name).matches()) {
                        possibleEmail = account.name;
                        break; // for reading only one email id of the user from accounts (changes after testing as History is not coming for several users)
                    }
                }
                url = new URL("http://fixmyroad.in/users.json?email_id=" + possibleEmail +"&curr_lat="+curr_lat+"&curr_long="+curr_long);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");

                try {
                    urlConnection.setDoOutput(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");

                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;


                        while ((line = bufferReader.readLine()) != null) {
                            stringBuilder.append(line).append("\n");
                        }
                        bufferReader.close();
                        result = stringBuilder.toString();
                        try {
                            // jArray = new JSONArray(result);
                            jObject = new JSONObject(result);
                            Log.d("GetConnect Aysnc task:", jObject.toString());

                        } catch (JSONException e) {
                            Log.e("log_tag", "Error parsing data " + e.toString());
                            Crittercism.logHandledException(e);
                        }
                        return jObject;



                } finally {
                    urlConnection.disconnect();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Crittercism.logHandledException(e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
                // catch event that there's no activity to handle intent
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d("DrawerItemClickListener", "Item Click");
            Log.d("Position", "" + position);
            selectItem(position);
        }
    }

    //
    private void selectItem(int position) {
        Intent intent;

        switch (position) {
            case 0:
                intent = new Intent(this, About.class);
                startActivity(intent);
                break;
            case 1:
                intent = new Intent(this, ListViewActivity.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, ListViewActivityCredits.class);
                startActivity(intent);
                break;
            case 3:
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
//                    startActivity(new Intent(Intent.ACTION_VIEW,
//                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                    Crittercism.logHandledException(e);
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }

                break;
            case 4:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Fed up with potholes in your city? Report bad roads in your locality with one touch. Download FixMyRoad http://bit.ly/FixMyRoad");
                sendIntent.putExtra(Intent.EXTRA_TEXT,APP_INVITE_SMS);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                break;

            case 5:
                intent = new Intent(this,Feedback.class);
               // intent.putExtra("jsonpotholes",potholes.toString());
                startActivity(intent);
                break;
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
//
//    private Bitmap DownloadImage(String paramString) {
//        Bitmap localBitmap = null;
//        try {
//            InputStream localInputStream = OpenHttpConnection(paramString);
//
//            localBitmap = BitmapFactory.decodeStream(localInputStream);
//            localInputStream.close();
//            return localBitmap;
//        } catch (IOException localIOException) {
//            localIOException.printStackTrace();
//        }
//        return localBitmap;
//    }
//
//    private InputStream OpenHttpConnection(String paramString)
//            throws IOException {
//        URLConnection localURLConnection = new URL(paramString).openConnection();
//        if (!(localURLConnection instanceof HttpURLConnection))
//            throw new IOException("Not an HTTP connection");
//        try {
//            HttpURLConnection localHttpURLConnection = (HttpURLConnection) localURLConnection;
//            localHttpURLConnection.setAllowUserInteraction(false);
//            localHttpURLConnection.setInstanceFollowRedirects(true);
//            localHttpURLConnection.setRequestMethod("GET");
//            localHttpURLConnection.connect();
//            int i = localHttpURLConnection.getResponseCode();
//            Object localObject = null;
//            if (i == 200) {
//                InputStream localInputStream = localHttpURLConnection.getInputStream();
//                localObject = localInputStream;
//            }
//            return (InputStream) localObject;
//        } catch (Exception localException) {
//        }
//        throw new IOException("Error connecting");
//    }

//    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
//        ViewHolder holder = null;
//
//        public MarkerInfoWindowAdapter() {
//        }
//
//        @Override
//        public View getInfoWindow(Marker marker) {
//            return null;
//        }
//
//        @Override
//        public View getInfoContents(Marker marker) {
//            Bundle bundle = new Bundle();
//            View v = getLayoutInflater().inflate(R.layout.infowindow_layout, null);
//            holder = new ViewHolder(v);
//            MyMarker myMarker = mMarkersHashMap.get(marker);
//            v.setLayoutParams(new RelativeLayout.LayoutParams(400, RelativeLayout.LayoutParams.WRAP_CONTENT));
////            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);
////            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);
////            Picasso.with(MapsActivity.this).load(myMarker.getmImagePath()).resize(100,100).into(v.getForeground(R.id.marker_icon));
//            if (myMarker != null) {
////                Log.d("Cheenu id of clicked marker", "" + myMarker.getmId());
//                if (myMarker.getmImagePath() != null) {
//                    String url = myMarker.getmImagePath();
//                    bundle.putString("imageUrl",url);
//
//                } else {
////                    markerIcon.setImageResource(R.drawable.ic_no_camera_capture_picture_image);
////                    Glide.with(MapsActivity.this).load(R.drawable.ic_no_camera_capture_picture_image).into(holder.Image);
//                    bundle.putString("imageUrl",null);
//                }
//
//                GPSTracker mGPS = new GPSTracker(MapsActivity.this);
//
//                if (mGPS.canGetLocation) {
//                    mGPS.getLocation();
//
//                    Double Lat = mGPS.getLatitude();
//                    Double Long = mGPS.getLongitude();
//                    double finalDistance = distance(Lat, Long, myMarker.getmLatitude(), myMarker.getmLongitude());
//                    bundle.putDouble("distance",finalDistance);
////                  holder.Distance.setText(new DecimalFormat("##.#").format(Double.valueOf(finalDistance)) + " kms away");
//
//                    // holder.Distance.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//                }
//                Intent intentMarker = new Intent(MapsActivity.this,InfoWindow_Marker.class);
//                intentMarker.putExtras(bundle);
//                startActivity(intentMarker);
////
//                Log.d("Cheenu","marker !=null");
//                return v;
//            }
//            else
//            {
//                Log.d("Cheenu","marker =null");
////                v = null;
//                 return v;
//            }
//
//
//        }
//    }
//

    public class MarkerClickListener implements GoogleMap.OnMarkerClickListener{


        @Override
        public boolean onMarkerClick(Marker marker) {
            Bundle bundle = new Bundle();
            MyMarker myMarker = mMarkersHashMap.get(marker);
//            v.setLayoutParams(new RelativeLayout.LayoutParams(400, RelativeLayout.LayoutParams.WRAP_CONTENT));
//            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);
//            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);
//            Picasso.with(MapsActivity.this).load(myMarker.getmImagePath()).resize(100,100).into(v.getForeground(R.id.marker_icon));
            Log.d("MarkerCLick", myMarker.toString());
            if (myMarker != null) {
//                Log.d("Cheenu id of clicked marker", "" + myMarker.getmId());
                if (myMarker.getmImagePath() != null) {
                    String url = myMarker.getmImagePath();
                    bundle.putString("imageUrl",url);

                } else {
//                    markerIcon.setImageResource(R.drawable.ic_no_camera_capture_picture_image);
//                    Glide.with(MapsActivity.this).load(R.drawable.ic_no_camera_capture_picture_image).into(holder.Image);
                       bundle.putString("imageURL",null);
                }

                String email = myMarker.getEmail();
                String timestamp = myMarker.getTimestamp();
                Log.d("MYMARKER TIMESTAMP",myMarker.getTimestamp());
                GPSTracker mGPS = new GPSTracker(MapsActivity.this);

                if (mGPS.canGetLocation) {
                    mGPS.getLocation();

                    Double Lat = mGPS.getLatitude();
                    Double Long = mGPS.getLongitude();
                    double finalDistance = distance(Lat, Long, myMarker.getmLatitude(), myMarker.getmLongitude());
                    bundle.putDouble("distance",finalDistance);
//                  holder.Distance.setText(new DecimalFormat("##.#").format(Double.valueOf(finalDistance)) + " kms away");

                    // holder.Distance.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                }

                bundle.putString("email",email);
                bundle.putString("timestamp",timestamp);
                Log.d("TIMESTAMP MARKER:",timestamp);
                Intent intentMarker = new Intent(MapsActivity.this,InfoWindow_Marker.class);
                intentMarker.putExtras(bundle);
                startActivity(intentMarker);

                Log.d("Cheenu","marker !=null");
//                return v;
            }
            else
            {
                Log.d("Cheenu","marker =null");
//                v = null;
//                 return v;
            }
            return true;
        }

//        public void dump()
//        {
//            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
//            View v = getLayoutInflater().inflate(R.layout.infowindow_layout, null);
//            holder = new ViewHolder(v);
//            holder.Distance.setText("computing distance from you...");
//            holder.Progress.setVisibility(View.VISIBLE);
//            MyMarker myMarker = mMarkersHashMap.get(marker);
//            v.setLayoutParams(new RelativeLayout.LayoutParams(400, RelativeLayout.LayoutParams.WRAP_CONTENT));
////            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);
////            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);
////            Picasso.with(MapsActivity.this).load(myMarker.getmImagePath()).resize(100,100).into(v.getForeground(R.id.marker_icon));
//            if (myMarker != null) {
////                Log.d("Cheenu id of clicked marker", "" + myMarker.getmId());
//                if (myMarker.getmImagePath() != null) {
//                    String url = myMarker.getmImagePath();
//
//                    if (url.contains("http")) {
//                        Log.d("Cheenu Image url", url);
//
//                        Glide.with(MapsActivity.this)
//                                .load(url)
//                                .listener(new RequestListener<String, GlideDrawable>() {
//                                    @Override
//                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                                        progressBar.setVisibility(View.GONE);
//                                        return false;
//                                    }
//
//                                    @Override
//                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                                        progressBar.setVisibility(View.GONE);
//                                        return false;
//                                    }
//                                })
//
//                                .override(400, 700)
//                                .into(holder.Image);
//                    }
//                    else {
//                        Log.d("Cheenu Image local", url);
////                        Picasso.with(MapsActivity.this)
////                                .load(new File(url))
////                                .resize(400, 400)
////                                .error(R.drawable.ic_photo_filter_black_18dp)
////                                .into(holder.Image);
//                        Glide.with(MapsActivity.this)
//                                .load(new File(url))
//                                .override(400, 700)
//                                .into(holder.Image);
//                    }
//                } else {
////                    markerIcon.setImageResource(R.drawable.ic_no_camera_capture_picture_image);
//                    Glide.with(MapsActivity.this).load(R.drawable.ic_no_camera_capture_picture_image).into(holder.Image);
//                }
//
//                GPSTracker mGPS = new GPSTracker(MapsActivity.this);
//
//                if (mGPS.canGetLocation) {
//                    mGPS.getLocation();
//
//                    Double Lat = mGPS.getLatitude();
//                    Double Long = mGPS.getLongitude();
//                    double finalDistance = distance(Lat, Long, myMarker.getmLatitude(), myMarker.getmLongitude());
//                    holder.Distance.setText(new DecimalFormat("##.#").format(Double.valueOf(finalDistance)) + " kms away");
//
//                    // holder.Distance.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//                }
//                Log.d("Cheenu","marker !=null");
//                return v;
//            }
//            else
//            {
//                Log.d("Cheenu","marker =null");
//                v = null;
//                return v;
//            }
//        }
    }
    private double distance(double lat1, double lng1, double lat2, double lng2) {
            double earthRadius = 6371; // in miles 3958.75, change to 6371 for kilometer output
            double dLat = Math.toRadians(lat2 - lat1);
            double dLng = Math.toRadians(lng2 - lng1);
            double sindLat = Math.sin(dLat / 2);
            double sindLng = Math.sin(dLng / 2);
            double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double dist = (earthRadius * c);
            return dist; // output distance, in MILES }
        }

//    private static class ViewHolder {
//        //private TextView Area;
//       // private TextView Timestamp;
//        private TextView Distance;
//        private ImageView Image;
//        private ProgressBar Progress;
//
//        public ViewHolder(View v) {
//            //  Area = (TextView) v.findViewById(R.id.area);
//            Progress = (ProgressBar) v.findViewById(R.id.progress);
//            Image = (ImageView) v.findViewById(R.id.marker_icon);
//         //   Timestamp = (TextView) v.findViewById(R.id.timestamp);
//            Distance = (TextView) v.findViewById(R.id.marker_label);
//        }
//    }

//    public long DifferenceDate(Date lastDate)
//    {
//        long diffMinutes = 0,diffHours=0,diffDays =0;
//        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//        Date currentDate=new Date(System.currentTimeMillis());
//        //milliseconds
//        long different = currentDate.getTime() - lastDate.getTime();
//
//        long secondsInMilli = 1000;
//        long minutesInMilli = secondsInMilli * 60;
//        long hoursInMilli = minutesInMilli * 60;
//        long daysInMilli = hoursInMilli * 24;
//
//        long elapsedDays = different / daysInMilli;
//        different = different % daysInMilli;
//
//        long elapsedHours = different / hoursInMilli;
//        different = different % hoursInMilli;
//
//        long elapsedMinutes = different / minutesInMilli;
//        different = different % minutesInMilli;
//
//        long elapsedSeconds = different / secondsInMilli;
//
//        System.out.printf(
//                "%d days, %d hours, %d minutes, %d seconds%n",
//                elapsedDays,
//                elapsedHours, elapsedMinutes, elapsedSeconds);
//
//        return elapsedSeconds;
//    }


    // Get data from Init
    class GetInit extends AsyncTask<Void,Void,JSONObject> {
        URL url;
        HttpURLConnection urlConnection;
        String result;
        JSONObject jObject= null;
        String possibleEmail = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            try {

                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+

                Account[] accounts = AccountManager.get(MapsActivity.this).getAccounts();
                // PERSMISSION ALERT: Account requires permission <uses-permission android:name="android.permission.GET_ACCOUNTS" />

                for (Account account : accounts) {
                    Log.d("account", account.toString());
                    if (emailPattern.matcher(account.name).matches()) {
                        possibleEmail = account.name;
                        break; // for reading only one email id of the user from accounts (changes after testing as History is not coming for several users)
                    }
                }
                url = new URL("http://fixmyroad.in/init.json?email_id=" + possibleEmail);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");

                try {
                    urlConnection.setDoOutput(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;

                    while ((line = bufferReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferReader.close();
                    result = stringBuilder.toString();
                    try{
                        // jArray = new JSONArray(result);
                        jObject= new JSONObject(result);
                        Log.d("GetInit Aysnc task:",jObject.toString());

                    }catch (JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Crittercism.logHandledException(e);
                    }
                    return jObject;

                } finally {
                    urlConnection.disconnect();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Crittercism.logHandledException(e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
        }
    }


    private void updateDatabasePothole()
    {
        db.updatePotholeSynced();
        Log.d("CheenuSync","updatePotholeSynced");
    }

    public long DifferenceDate(Date lastDate)
    {
        long diffMinutes = 0,diffHours=0,diffDays =0;
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date currentDate=new Date(System.currentTimeMillis());
        //milliseconds
        long different = currentDate.getTime() - lastDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

        return elapsedSeconds;
    }

}