package com.pothole.fixmyroad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Surabhi Agrawal on 10/1/2016.
 */
public class MyAlarmReceiver extends BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;
//    public static final String ACTION = "com.codepath.example.servicesdemo.alarm";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, PotholeService.class);
        i.putExtra("foo", "bar");
        context.startService(i);
    }
}
