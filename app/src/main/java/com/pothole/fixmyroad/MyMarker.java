package com.pothole.fixmyroad;
//package anubhav.hoolia;

import android.util.Log;

public class MyMarker {
    private int mId;
    private String mImagePath;
    private Double mLatitude;
    private Double mLongitude;
    private String email;
    private String timestamp;

    public MyMarker(int id, String imagePath, Double latitude, Double longitude,String email,String timestamp) {
        this.mId = id;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
        this.mImagePath = imagePath;
        this.email =email;
        this.timestamp = timestamp;

    }

    public int getmId() {
        return this.mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmImagePath() {
        return this.mImagePath;
    }

    public void setmImagePath(String ImagePath) {
        this.mImagePath = ImagePath;
    }

    public Double getmLatitude() {
        return this.mLatitude;
    }

    public void setmLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public Double getmLongitude() {
        return this.mLongitude;
    }

    public void setmLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getEmail()
    {
        Log.d("Email: getEmail, this.email", this.email);
        return this.email;
    }

    public void setEmail(String _email)
    {
        Log.d("Email: setEmail, _email", _email);
        this.email = _email;
        Log.d("Email: setEmail, this.email", this.email);
    }

    public String getTimestamp()
    {
        return this.timestamp;
    }

    public void setTimestamp(String _timestamp)
    {
        this.timestamp= _timestamp;
    }
}

