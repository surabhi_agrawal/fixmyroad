package com.pothole.fixmyroad;

import com.crittercism.app.Crittercism;

import java.sql.Date;
import java.text.SimpleDateFormat;

//package anubhav.hoolia;

public class OfficeAddress_DB {
    boolean flag;
    int id;
    double officeLatitude;
    double officeLongitude;
    String timeStamp;

    public OfficeAddress_DB(double _officeLatitude, double _officeLongitude, boolean _flag) {
        this.officeLatitude = _officeLatitude;
        this.officeLongitude = _officeLongitude;
        this.flag = _flag;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int _id) {
        this.id = _id;
    }

    public String getTimestamp() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
            return null;
        }
    }

    public void setTimeStamp(String _timeStamp) {
        this.timeStamp = _timeStamp;
    }

    public double getHomeLatitude() {
        return this.officeLatitude;
    }

    public void setHomeLatitude(double _officeLatitude) {
        this.officeLatitude = _officeLatitude;
    }

    public double getHomeLongitude() {
        return this.officeLongitude;
    }

    public void setHomeLongitude(double _officeLongitude) {
        this.officeLongitude = _officeLongitude;
    }

    public boolean getFlag() {
        return this.flag;
    }

    public void setFlag(boolean _flag) {
        this.flag = _flag;
    }
}
