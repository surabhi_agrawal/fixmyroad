package com.pothole.fixmyroad;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.crittercism.app.Crittercism;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by hp on 01-Mar.
 */
public class PopUp extends AppCompatActivity{
    Button btnCamera;
    ImageView mImageView;
    String ba1;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    String deviceID,pathOfImage;
    double latitude;
    double longitude;
    String possibleEmail;
    public static final int POTHOLE_SELECT = 1;
    public static JSONObject jPothole = null;
    private ProgressDialog pDialog;

    Intent mServiceIntent;
    CapitalReceiver mReceiver;
    IntentFilter mFilter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_window);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .6));

        btnCamera = (Button) findViewById(R.id.btnCamera);
        mImageView = (ImageView) findViewById(R.id.imageView1);

        // Checking if internet and GPS is on or not if not it will ask you to Enable it
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            Crittercism.logHandledException(ex);
        }

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(
                    this.getResources().getString(R.string.open_location_settings),
                    new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                        Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        //get gps
                    }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.setMessage(this.getResources().getString(R.string.network_not_enabled));
            alert.setPositiveButton(this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            alert.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            alert.show();
        }


        //   btnUpload = (Button) findViewById(R.id.btnUpload);
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager lm = (LocationManager)
                        PopUp.this.getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                boolean network_enabled = false;

                gps_enabled = false;
                network_enabled = false;

                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch(Exception ex) {
                    Crittercism.logHandledException(ex);
                }

                try {
                    network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                } catch(Exception ex) {
                    Crittercism.logHandledException(ex);
                }

                if(!gps_enabled && !network_enabled) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(PopUp.this);
                    dialog.setMessage(PopUp.this.getResources().getString(R.string.gps_network_not_enabled));
                    dialog.setPositiveButton(PopUp.this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    dialog.setNegativeButton(PopUp.this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    dialog.show();
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(PopUp.this);
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                // Check if Internet present
                if (!cd.isConnectingToInternet()) {
                    // Internet Connection is not present
                    alert.setMessage(PopUp.this.getResources().getString(R.string.network_not_enabled));
                    alert.setPositiveButton(PopUp.this.getResources().getString(R.string.open_network_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent( Settings.ACTION_WIFI_SETTINGS);
                            startActivity(myIntent);
                            //get gps
                        }
                    });
                    alert.setNegativeButton(PopUp.this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    alert.show();
                }
                else
                {
                    Log.d("Image Func()", "captureImage()");
                    Bundle bundle = getIntent().getExtras();
                    deviceID = bundle.getString("Device_id");
                    latitude = bundle.getDouble("signup_lat");
                    longitude = bundle.getDouble("signup_long");
                    btnTakePhotoClicked(v);
                    pathOfImage = fileUri.getPath();
                    // Thank you  message after Marking pothole
                    //   Toast.makeText(PopUp.this, "Thank you for marking pothole",Toast.LENGTH_LONG).show();
                    Log.d("Insert:Pothole ", "Inserting ..");
                    //Potholes_DB addPothole = new Potholes_DB(latitude,longitude,null,pathOfImage,deviceID,possibleEmail);
                }
            }
        });

//        btnCamera.callOnClick();
        btnCamera.performClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE){
            if(resultCode == RESULT_OK){
//                Log.d("Surabhi", fileUri.getPath());
//                Bitmap bm = BitmapFactory.decodeFile(fileUri.getPath());
//                Log.d("Image File size", ""+(fileUri.getPath()).length());
//                mImageView.setImageBitmap(bm);
//                ByteArrayOutputStream bao = new ByteArrayOutputStream();
//                bm.compress(Bitmap.CompressFormat.JPEG,80, bao); // Changed from 50 to 100 for the checking the quality of images sent to server
//
//                byte[] by = bao.toByteArray();
//                ba1 = Base64.encodeToString(by, Base64.DEFAULT);
//
//                Log.d("Image into base64",bm.toString());

                // reading email id from Device
                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                Pattern phonePattern = Patterns.PHONE;

                Account[] accounts = AccountManager.get(this).getAccounts();
                // PERSMISSION ALERT: Account requires permission <uses-permission android:name="android.permission.GET_ACCOUNTS" />

                for (Account account : accounts) {
                    Log.d("account",account.toString());
                    if (emailPattern.matcher(account.name).matches()) {
                        possibleEmail = account.name;
                        break; // for reading only one email id of the user from accounts (changes after testing as History is not coming for several users)
                    }
                }

                JSONObject parentObj = new JSONObject();
                JSONObject childObj = new JSONObject();

                try{
//                    childObj.put("image",ba1);
                    childObj.put("image",fileUri.getPath());
                    childObj.put("email_id",possibleEmail );
                    childObj.put("signup_lat",latitude);
                    childObj.put("signup_long",longitude);
                    // childObj.put("Device_id",deviceID);
                    parentObj.put("user",childObj);
                    Log.d("Json Object",parentObj.toString());
                }catch (Exception e) {
                    Toast.makeText(this, "Something went wrong. Please try again.",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }

//                mServiceIntent = new Intent(getApplicationContext(),PotholeService.class);
                // Upload image to server
//                JSONObject jPothole = null;
//                UploadToServer upload = new UploadToServer(); //commented as of now to test PotholeService
//                try {
//
//                    jPothole = upload.execute(String.valueOf(parentObj)).get();
//                    upload.execute(String.valueOf(parentObj)); //commented as of now to test PotholeService
//                      mServiceIntent.putExtra("parentObj",parentObj.toString());
//                      startService(mServiceIntent);

                    // Instantiating BroadcastReceiver
//                    mReceiver = new CapitalReceiver();

                    // Creating an IntentFilter with action
//                    mFilter = new IntentFilter(Constants.BROADCAST_ACTION);

                    // Registering BroadcastReceiver with this activity for the intent filter
//                    LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mReceiver, mFilter);
//                    pDialog.dismiss();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Crittercism.logHandledException(e);
//                }
                Toast.makeText(PopUp.this, "Great job reporting this issue. Thanks a bunch :)",Toast.LENGTH_LONG).show();

                Bundle bundle = new Bundle();
                bundle.putDouble("signup_lat",latitude);
                bundle.putDouble("signup_long",longitude);
                bundle.putString("emailid", possibleEmail);
                bundle.putString("image", fileUri.getPath());
                try {
                    if (jPothole != null) {
                        bundle.putInt("id", jPothole.getInt("user_id"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Crittercism.logHandledException(e);
                }
//                Intent intent = new Intent(PopUp.this,MapsActivity.class);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                if (getParent() == null) {
                    Log.d("Cheenu", "getParent() == null:");
                    Log.d("Cheenu", "PopUp::POTHOLE_SELECT:"+POTHOLE_SELECT);
                    Log.d("Cheenu", "PopUp::RESULT_OK:"+Activity.RESULT_OK);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
//                else {
//                    Log.d("Cheenu", "getParent() IS NOT null:");
//                    Log.d("Cheenu", "PopUp::POTHOLE_SELECT:"+POTHOLE_SELECT);
//                    Log.d("Cheenu", "PopUp::RESULT_CANCELED:"+Activity.RESULT_CANCELED);
//
//                    getParent().setResult(Activity.RESULT_OK, intent);
//                }
//                //setResult(RESULT_OK,intent);
//                supportFinishAfterTransition(); // for closing the pop up window once the upload image/ data gets completed
                finish();
            }
            else if(resultCode == RESULT_CANCELED){
//                Log.d("Cheenu", "PopUp::resultCode == RESULT_CANCELED");
//                Log.d("Cheenu", "PopUp::POTHOLE_SELECT:"+POTHOLE_SELECT);
//                Log.d("Cheenu", "PopUp::RESULT_CANCELED:"+RESULT_CANCELED);
                Toast
                     .makeText(this,"Missed taking picture? Please try again.",Toast.LENGTH_LONG)
                     .show();
                finish();
            }else {
                Toast.makeText(this, "Camera failed. Please try again", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
       // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && cameraAccepted && storageAccepted) {
                      //  Toast.makeText(PopUp.this, "Permission Granted, Now you can access location data/camera/storage", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(PopUp.this, "Oops! Please provide storage permission from mobile settings.", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        }
                                );
                                return;
                            }
                        }
                    }
                }

            break;
        }
    }

    public void btnTakePhotoClicked(View v){

        //Checking camera persmission

//        if(ContextCompat.checkSelfPermission(PopUp.this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED)
//        {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(PopUp.this,
//                    Manifest.permission.CAMERA)){
//
//
//                Toast.makeText(PopUp.this, "Camera Capture.Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//            }
//
//            else {
//
//                // No explanation needed, we can request the permission.
//
//                ActivityCompat.requestPermissions(PopUp.this,
//                        new String[]{Manifest.permission.CAMERA},
//                        PERMISSION_REQUEST_CODE);
//                // PERMISSION_REQUEST_CODE is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        }


        if(!checkPermission())
        {
            requestPermission();
        }
        else
        {
           // Toast.makeText(PopUp.this, "Permission already granted.", Toast.LENGTH_LONG).show();
        }

        //Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file=getOutputMediaFile(MEDIA_TYPE_IMAGE);
        fileUri = Uri.fromFile(file);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri);
        startActivityForResult(cameraIntent,CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    public boolean checkPermission(){

        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(),WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, CAMERA,WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(PopUp.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }



    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "Pothole");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new java.util.Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public class UploadToServer extends AsyncTask<String,Void,JSONObject> {



        //ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Cheenu", "onPreExecute");
            pDialog = new ProgressDialog(PopUp.this);
            pDialog.setTitle("Processing...");
            pDialog.setMessage("Please wait.");
            pDialog.setCancelable(false);
            pDialog.setIndeterminate(true);
            pDialog.show();
//            Log.e("onPreExecute","called" + pDialog);
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            HttpURLConnection urlConnection;
            String jsonData;
            jsonData = params[0];
            String result;
            JSONObject jObject = null;
            try {
                url = new URL("http://fixmyroad.in/users");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");

                try {
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    //    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                    writer.write(jsonData);
                    writer.flush();
                    writer.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    reader.close();
                    result = stringBuilder.toString();
                    int responseCode = urlConnection.getResponseCode();
                    String responseText = urlConnection.getResponseMessage();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        Log.d("Cheenu POST request worked", "HTTPOK");

                    } else
                        Log.d("Cheenu POST request not worked", responseText);
                    try{
                        // jArray = new JSONArray(result);
                        jObject= new JSONObject(result);
                        Log.d("GetConnect Aysnc task:",jObject.toString());

                    }catch (JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Crittercism.logHandledException(e);
                    }
                    return jObject;
                    // writeStream(out);
                } finally {
                    urlConnection.disconnect();
                }
            }catch (Exception e){
                e.printStackTrace();
                Crittercism.logHandledException(e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            jPothole = jsonObject;

            pDialog.dismiss();
//            progressBar.setVisibility(View.GONE);
//            try{
//                Log.d("Cheenu","DIALOG onPostExecute");
//
////                if(pDialog.isShowing())
////                {
////                    Log.d("Cheenu", "DIALOG onPostExecute, isShowing()=true");
////                    pDialog.dismiss();
////                }
////                pDialog.setCancelable(false);
////            }
////            catch(Exception e)
////            {
////                e.printStackTrace();
////                Crittercism.logHandledException(e);
////            }
        }
    }



    public class UploadToServerWithOutProgress extends AsyncTask<String,Void,JSONObject> {

//        private ProgressDialog pDialog = new ProgressDialog(PopUp.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("Cheenu", "onPreExecute");
            //pDialog.setMessage("Please wait..");
            //pDialog.setIndeterminate(false);
            //pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            HttpURLConnection urlConnection;
            String jsonData;
            jsonData = params[0];
            String result;
            JSONObject jObject = null;
            try {
                url = new URL("http://fixmyroad.in/users");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");

                try {
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    //    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                    writer.write(jsonData);
                    writer.flush();
                    writer.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    reader.close();
                    result = stringBuilder.toString();
                    int responseCode = urlConnection.getResponseCode();
                    String responseText = urlConnection.getResponseMessage();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        Log.d("Cheenu POST request worked", "HTTPOK");

                    } else
                        Log.d("Cheenu POST request not worked", responseText);
                    try{
                        // jArray = new JSONArray(result);
                        jObject= new JSONObject(result);
                        Log.d("GetConnect Aysnc task:",jObject.toString());

                    }catch (JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Crittercism.logHandledException(e);
                    }
                    return jObject;
                    // writeStream(out);
                } finally {
                    urlConnection.disconnect();
                }
            }catch (Exception e){
                e.printStackTrace();
                Crittercism.logHandledException(e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            jPothole = jsonObject;

//            try{
//                Log.d("Cheenu","DIALOG onPostExecute");
//
////                if(pDialog.isShowing())
////                {
////                    Log.d("Cheenu", "DIALOG onPostExecute, isShowing()=true");
////                    pDialog.dismiss();
////                }
////                pDialog.setCancelable(false);
////            }
////            catch(Exception e)
////            {
////                e.printStackTrace();
////                Crittercism.logHandledException(e);
////            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    // Defining a BroadcastReceiver
    private class CapitalReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String capital = intent.getStringExtra("result");

        }
    }
}