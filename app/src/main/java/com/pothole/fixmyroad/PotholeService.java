package com.pothole.fixmyroad;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.util.Base64;
import android.util.Log;

import com.crittercism.app.Crittercism;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Surabhi Agrawal on 9/26/2016.
 */
public class PotholeService extends IntentService {
    public static final String LOG_TAG = "PotholeService";
    private int resultF = Activity.RESULT_CANCELED;
    DatabaseHandler db = new DatabaseHandler(this);

    // Defines and instantiates an object for handling status
    public PotholeService() {
        super("PotholeService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        String result;
        URL url = null;
        String pathImage = null;
        String email = null;
        double signup_lat = 0, signup_long =0;
        JSONObject resultJson= null;
        String ba1;
        String deviceID = null;
        String timeStamp = null;
        String comments = null;
        HttpURLConnection urlConnection = null;
        JSONObject user = new JSONObject();
        JSONObject finalObj = new JSONObject();

        String finalJson = null;
//        Bundle b = intent.getExtras();
//        String jObject = b.getString("parentObj");
//        Log.d("RequestJson", jObject);
        try {
            url = new URL("http://fixmyroad.in/users");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        ArrayList<Potholes_DB> allPotholes = (ArrayList<Potholes_DB>) db.getPotholesIsSync();
        Log.d("ALl POTHOLES IN SERVICE",allPotholes.toString());
        JSONArray jsArray = new JSONArray();

        if(allPotholes.size() != 0) {


            for (int i = 0; i < allPotholes.size(); i++) {
                JSONObject jGroup = new JSONObject();
                try {

                    jGroup.put("device_id", allPotholes.get(i).getDeviceid());
                    jGroup.put("email_id", allPotholes.get(i).getEmailId());
                    jGroup.put("signup_lat", allPotholes.get(i).getlatitude());
                    jGroup.put("signup_long", allPotholes.get(i).getlongitude());
//                jGroup.put("pathofimage", allPotholes.get(i).getPathofImage());
                    Bitmap bm;
                    Log.d("Image File",allPotholes.get(i).getPathofImage());
                    Log.d("Image File size", "" + (allPotholes.get(i).getPathofImage()).length());
                    if((allPotholes.get(i).getPathofImage()).contains("http"))
                    {
                        Log.d("Downloaded from Internet","");
                        bm = BitmapFactory.decodeStream((InputStream)new URL(allPotholes.get(i).getPathofImage()).getContent());
                    }
                    else
                    {
                        Log.d("Downloaded from Local","");
                        bm = BitmapFactory.decodeFile(allPotholes.get(i).getPathofImage());
                    }

                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 20, bao); // Changed from 50 to 100 for the checking the quality of images sent to server
                    byte[] by = bao.toByteArray();
                    ba1 = Base64.encodeToString(by, Base64.DEFAULT);
                    Log.d("Bytes Of Image",by.length +"");
                    jGroup.put("image", ba1);
                    jGroup.put("timestamp", allPotholes.get(i).getTimestamp());
                    jGroup.put("comments", allPotholes.get(i).getComments());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                jsArray.put(jGroup);
            }
            try {
                finalObj.put("is_bulk_upload", true);
                finalObj.put("user", jsArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

//                finalObj.put("user",jGroup);
            finalJson = finalObj.toString();
            Log.d("FinalJSON", finalObj.toString());

            try {
                urlConnection = (HttpURLConnection) url.openConnection();
            } catch (IOException e1) {
                e1.printStackTrace();
            }


            try {

                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                //    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                writer.write(finalJson);
                writer.flush();
                writer.close();
//            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//            StringBuilder stringBuilder = new StringBuilder();
//            String line;
//            while ((line = reader.readLine()) != null) {
//                stringBuilder.append(line).append("\n");
//            }
//            reader.close();
//            result = stringBuilder.toString();
                int responseCode = urlConnection.getResponseCode();
                String responseText = urlConnection.getResponseMessage();
                Log.d("Service Response Code", responseCode+"");
                if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                    Log.d("Cheenu service POST request worked", "HTTPOK");
                    Log.d("Response COde",responseCode +"");
                    resultF = Activity.RESULT_OK;
                } else {
                    Log.d("Cheenu service POST request not worked", responseText);
                    resultF = Activity.RESULT_CANCELED;
                }
                try {
                    // jArray = new JSONArray(result);
//                resultJson= new JSONObject(result);
                    Log.d("GetConnect Aysnc task:", finalJson);

                    // Sucessful finished


                } catch (Exception e) {
                    Log.e("log_tag", "Error parsing data " + e.toString());
                    Crittercism.logHandledException(e);
                }
//            return jObject;
                // writeStream(out);
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }

            Bundle extras = intent.getExtras();
            if (extras != null) {
                Messenger messenger = (Messenger) extras.get("MESSENGER");
                Message msg = Message.obtain();
                msg.arg1 = resultF;
                msg.obj = resultJson;
                Log.d("RESULTF",resultF +"");
                Log.d("ARG1",msg.arg1+"");
                try {
                    messenger.send(msg);
                } catch (android.os.RemoteException e1) {
                    Log.w(getClass().getName(), "Exception sending message", e1);
                }

            }

        }

        else
        {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                Messenger messenger = (Messenger) extras.get("MESSENGER");
                Message msg = Message.obtain();
                msg.arg1 = resultF;
                msg.obj = resultJson;
                Log.d("RESULTF",resultF +"");
                Log.d("ARG1",msg.arg1+"");
                try {
                    messenger.send(msg);
                } catch (android.os.RemoteException e1) {
                    Log.w(getClass().getName(), "Exception sending message", e1);
                }

            }
        }
    }
}

