package com.pothole.fixmyroad;

import com.crittercism.app.Crittercism;

public class Potholes_DB {
    String comments;
    String deviceid;
    int id;
    double lat;
    double longi;
    String pathofimage;
    String timestamp;
    String emailId;
    int isSync;

    public Potholes_DB()
    {

    }

    public Potholes_DB(String _timestamp,double _lat, double _long, String _pathofimage, String _comments, String _deviceid, String _emailId, int _isSync) {
        this.timestamp = _timestamp;
        this.lat = _lat;
        this.longi = _long;
        this.pathofimage = _pathofimage;
        this.comments = _comments;
        this.deviceid = _deviceid;
        this.emailId = _emailId;
        this.isSync = _isSync;
    }

    public Potholes_DB(double _lat, double _long, String _pathofimage, String _comments, String _deviceid, String _emailId, int _isSync) {
        this.lat = _lat;
        this.longi = _long;
        this.pathofimage = _pathofimage;
        this.comments = _comments;
        this.deviceid = _deviceid;
        this.emailId = _emailId;
        this.isSync = _isSync;
    }


    public int getID() {
        return this.id;
    }

    public void setID(int _id) {
        this.id = _id;
    }

    public void setTimestamp(String _timestamp) {
        this.timestamp = _timestamp;
    }

    public String getTimestamp() {
        try {
//            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
              return this.timestamp;
        } catch (Exception e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
            return null;
        }
    }

    public double getlatitude() {
        return this.lat;
    }

    public void setlatitude(double _lat) {
        this.lat = _lat;
    }

    public double getlongitude() {
        return this.longi;
    }

    public void setlongitude(double longi) {
        this.longi = longi;
    }

    public String getPathofImage() {
        return this.pathofimage;
    }

    public void setPathofImage(String _pathofimage) {
        this.pathofimage = _pathofimage;
    }

    public void setComments(String _comments) {
        this.comments = _comments;
    }

    public String getComments() {
        return this.comments;
    }

    public void setDeviceID(String _deviceid) {
        this.deviceid = _deviceid;
    }

    public String getDeviceid() {
        return this.deviceid;
    }

    public String getEmailId()
    {
        return this.emailId;
    }

    public void setEmailId(String _emailId)
    {
        this.emailId = _emailId;
    }

    public int getIsSync()
    {
        return this.isSync;
    }

    public void setIsSync(int _isSync)
    {
        this.isSync = _isSync;
    }
}
