package com.pothole.fixmyroad;

/**
 * Created by hp on 7/30/2016.
 */
public class RowItem_Credits {
    private String email;
    private int count_potholes;

    public RowItem_Credits()
    {

    }
    public RowItem_Credits(String _email, int count_potholes)
    {
        this.email = _email;
        this.count_potholes = count_potholes;
    }

    public String getEmail()
    {
        return this.email;
    }

    public void setEmail(String _email)
    {
        this.email = _email;
    }

    public int getCount_potholes()
    {
        return this.count_potholes;
    }

    public void setCount_potholes(int _countpotholes)
    {
        this.count_potholes = _countpotholes;
    }

}
