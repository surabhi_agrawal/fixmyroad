package com.pothole.fixmyroad;

        import com.crittercism.app.Crittercism;

/**
 * Created by hp on 7/28/2016.
 */
public class RowItem_History {
    private double latitude;
    private double longitude;
    private String imageUrl;
    private String email;
    private String timeStamp;
    private String address;
    private int isSync;

    final String DATEFORMAT = "yyyy-MM-dd";
//    2016-07-28T08:41:48.285Z

    public RowItem_History()
    {

    }
    public RowItem_History(double _latitude, double _longitude, String _imageUrl, String _email, String _timeStamp,String _address,int _isSync)
    {
        this.latitude= _latitude;
        this.longitude = _longitude;
        this.imageUrl = _imageUrl;
        this.email = _email;
        this.timeStamp = _timeStamp;
        this.address = _address;
        this.isSync =  _isSync;

    }

    public RowItem_History(double _latitude, double _longitude, String _imageUrl, String _email,String _timestamp,int _isSync)
    {
        this.latitude= _latitude;
        this.longitude = _longitude;
        this.imageUrl = _imageUrl;
        this.email = _email;
        this.timeStamp = _timestamp;
        this.isSync = _isSync;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double _latitude)
    {
        this.latitude = _latitude;
    }
    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double _longitude)
    {
        this.longitude = _longitude;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public void setImageUrl(String _imageUrl)
    {
        this.imageUrl = _imageUrl;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String _email)
    {
        this.email = _email;
    }

    public String getTimeStamp(){
        try {
            return this.timeStamp;
        } catch (Exception e) {
            e.printStackTrace();
            Crittercism.logHandledException(e);
            return null;
        }
    }
    public void setTimeStamp(String _timeStamp){
        //SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        this.timeStamp = _timeStamp;

    }

    public String getAddress()
    {
        return this.address;
    }

    public void setAddress(String _address)
    {
        this.address = _address;
    }

    public int getIsSync()
    {
        return this.isSync;
    }
    public void setIsSync(int _isSync)
    {
        this.isSync = _isSync;
    }
}
