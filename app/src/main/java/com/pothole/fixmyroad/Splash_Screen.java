//package com.pothole.fixmyroad;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Log;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.concurrent.ExecutionException;
//
///**
// * Created by hp on 8/1/2016.
// */
//public class Splash_Screen extends Activity {
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.splash_screen);
//        new UpdateRunnable(this, new Handler()).start();
//        ImageView imgLogo= (ImageView) findViewById(R.id.imgLogo);
//        TextView txtName = (TextView) findViewById(R.id.txtName);
////        Bitmap bm = BitmapFactory.decodeResource(this.getResources(),R.drawable.road_filled_50);
////        imgLogo.setImageBitmap(bm);
//        imgLogo.setBackgroundResource(R.drawable.road_filled_50);
//        try {
//            new GetConnects().execute().get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    class GetConnects extends AsyncTask<Void,Void,Void> {
//        URL url;
//        HttpURLConnection urlConnection;
//        String result;
//        JSONArray jArray;
//        JSONObject jObject;
//
//        private ProgressDialog dialog;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            dialog = new ProgressDialog(Splash_Screen.this);
//            // Set progress dialog title
////        dialog.setTitle("Getting JSON DATA");
//            // Set progress dialog message
//            dialog.setMessage("Submitting Feedback...");
//            dialog.setIndeterminate(false);
//            // Show progress dialog
//            dialog.show();
//        }
//            @Override
//        protected Void doInBackground(Void... params) {
//
//            try {
//                url = new URL("http://fixmyroad.in/users.json");
//                urlConnection = (HttpURLConnection) url.openConnection();
//                urlConnection.setRequestMethod("GET");
//
//                try {
//                    urlConnection.setDoOutput(false);
//                    urlConnection.setRequestProperty("Content-Type", "application/json");
//                    urlConnection.setRequestProperty("Accept", "application/json");
//                    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String line;
//                    while ((line = bufferReader.readLine()) != null) {
//                        stringBuilder.append(line).append("\n");
//                    }
//                    bufferReader.close();
//                    result = stringBuilder.toString();
//                    try{
//                        // jArray = new JSONArray(result);
//                        jObject= new JSONObject(result);
//                        Log.d("GetConnect Aysnc task:", jObject.toString());
//                    }catch (JSONException e){
//                        Log.e("log_tag", "Error parsing data "+e.toString());
//                    }
//                    //return jObject;
//
//                } finally {
//                    urlConnection.disconnect();
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                return null;
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            dialog.dismiss();
//            // After completing http call
//            // will close this activity and lauch main activity
//
//            Intent i = new Intent(Splash_Screen.this, MapsActivity.class);
//            i.putExtra("jsonpotholes", jObject.toString());
//            startActivity(i);
//            finish();
//        }
//
//    }
//}
